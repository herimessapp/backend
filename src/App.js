import React, { useState } from "react";
// import logo from './logo.svg';
// import './App.css';
import LoginEmail from "./LoginEmail";
import LoginOtp from "./LoginOtp";
import DashboardSuper from "./Module/messappsuper/DashboardSuper";
// import DashboardRead from "./Module/messappread/DashboardRead";
// import DashboardOperator from "./Module/operator/DashboardOperator";
import NoSsr from "@material-ui/core/NoSsr";

function App() {
  const [user, setUser] = useState({ email: "", role: "", otp: false });
  if (user.email === "") {
    return (
      <NoSsr>
        <LoginEmail user={user} setUser={setUser} />
      </NoSsr>
    );
  } else if (user.email !== "" && user.role !== "" && user.otp === false) {
    return (
      <NoSsr>
        <LoginOtp user={user} setUser={setUser} />
      </NoSsr>
    );
  } else {
    console.log(user.role);
    switch (user.role) {
      case "messappcrud":
        return <DashboardSuper user={user} setUser={setUser} />;
      case "messappreadonly":
        return <div>DashboardReadOnly</div>;
      case "rekammedic":
        return <div>DashboardRekamMedic</div>;
      case "doctor":
        return <div>DashboardDoctor</div>;
      case "apotek":
        return <div>DashboardApotek</div>;
      case "antrian":
        return <div>DashboardAntrian</div>;
      case "tindakan":
        return <div>DashboardTindakan</div>;
      default:
        return <div>messappreadonly</div>;
    }
  }
}

// function App() {
//   return <DashboardSuper />;
// }

export default App;
