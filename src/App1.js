import React, { useState } from "react";
// import logo from './logo.svg';
// import './App.css';
import LoginEmail from "./LoginEmail";
import LoginOtp from "./LoginOtp";
import DashboardSuper from "./Module/messappsuper/DashboardSuper";
import NoSsr from "@material-ui/core/NoSsr";

function App() {
  const [user, setUser] = useState({ email: "", otp: false });
  if (user.email === "") {
    return (
      <NoSsr>
        <LoginEmail user={user} setUser={setUser} />
      </NoSsr>
    );
  } else if (user.email !== "" && user.otp === false) {
    return (
      <NoSsr>
        <LoginOtp user={user} setUser={setUser} />
      </NoSsr>
    );
  } else if (user.role === 1 && user.otp) {
    return (
      <NoSsr>
        <DashboardSuper user={user} />
      </NoSsr>
    );
  }
}
export default App;
