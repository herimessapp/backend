import React from "react";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { useHistory } from "react-router-dom";
import "remixicon/fonts/remixicon.css";
const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    textAlign: "left",
    justifyContent: "space-between",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  fontKlinik: {
    marginLeft: "8px",
  },
}));

export default function DrawerDoctor(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const history = useHistory();

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <i class="ri-dashboard-line"></i>
          </IconButton>
          <Typography variant="h6" className={classes.fontKlinik}>
            Klinik
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
          <Typography variant="p" component="p">
            Klinik
          </Typography>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <i class="ri-arrow-right-line"></i>
            ) : (
              <i class="ri-arrow-left-line"></i>
            )}
          </IconButton>
        </div>
        <Divider />
        <List>
          <ListItem
            button
            onClick={() => {
              //props.setIsDrawerOpen(false)
              history.push("/");
            }}
          >
            <ListItemIcon>
              <i class="ri-dashboard-line"></i>
            </ListItemIcon>
            <ListItemText primary="Beranda" />
          </ListItem>
          <ListItem
            button
            onClick={() => {
              //props.setIsDrawerOpen(false)
              history.push("/pasien");
            }}
          >
            <ListItemIcon>
              <i class="ri-stethoscope-fill"></i>
            </ListItemIcon>
            <ListItemText primary="Data Pasien" />
          </ListItem>
          <ListItem
            button
            onClick={() => {
              //props.setIsDrawerOpen(false)
              history.push("/konsultasi");
            }}
          >
            <ListItemIcon>
              <i class="ri-health-book-line"></i>
            </ListItemIcon>
            <ListItemText primary="Data Konsultasi" />
          </ListItem>
          <ListItem
            button
            onClick={() => {
              //props.setIsDrawerOpen(false)
              history.push("/setting-antrian");
            }}
          >
            <ListItemIcon>
              <i class="ri-file-settings-line"></i>
            </ListItemIcon>
            <ListItemText primary="Setting Antrian" />
          </ListItem>
          <ListItem
            onClick={() => {
              //props.setIsDrawerOpen(false)
              history.push("/tindakan");
            }}
            button
          >
            <ListItemIcon>
              <i class="ri-file-list-line"></i>
            </ListItemIcon>
            <ListItemText primary="Data Tindakan" />
          </ListItem>
          
          <ListItem button>
            <ListItemIcon>
              <i class="ri-timer-line"></i>
            </ListItemIcon>
            <ListItemText primary="Jadwal Konsultasi" />
          </ListItem>

          <ListItem
            button
            onClick={() => {
              //props.setIsDrawerOpen(false)
              history.push("/antrian");
            }}
          >
            <ListItemIcon>
              <i class="ri-file-list-3-line"></i>
            </ListItemIcon>
            <ListItemText primary="Antrian Konsultasi" />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <i class="ri-list-settings-line"></i>
            </ListItemIcon>
            <ListItemText primary="Jadwal Tindakan" />
          </ListItem>
          <ListItem
            onClick={() => {
              //props.setIsDrawerOpen(false)
              history.push("/rekam-medis");
            }}
            button
          >
            <ListItemIcon>
              <i class="ri-first-aid-kit-line"></i>
            </ListItemIcon>
            <ListItemText primary="Rekam Medis" />
          </ListItem>
          <ListItem
            onClick={() => {
              //props.setIsDrawerOpen(false)
              history.push("/resep");
            }}
            button
          >
            <ListItemIcon>
              <i class="ri-flask-line"></i>
            </ListItemIcon>
            <ListItemText primary="Data Resep" />
          </ListItem>
        </List>
      </Drawer>
    </div>
  );
}
