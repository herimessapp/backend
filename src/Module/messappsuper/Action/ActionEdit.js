import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";

import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { MenuItem } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function ActionEdit() {
  const classes = useStyles();
  const history = useHistory();
  const [kodeKlinik, setKodeKlinik] = useState("");
  const [getCodeKlinik, setGetCodeKlinik] = useState([]);
  const [namaTindakan, setNamaTindakan] = useState("");
  const [typeTindakan, setTypeTindakan] = useState("");
  const [diskripsiTindakan, setDiskripsi] = useState("");
  const [harga, setHarga] = useState("");
  const [isSuccess, setIsSuccess] = useState(false);
  const [isErr, setIsErr] = useState(false);
  const [msg, setMsg] = useState("");
  const [isLoaded, setIsLoaded] = useState(false);
  const { idTindakan } = useParams();

  const { register, handleSubmit } = useForm();

  let addActionData = {
    clinic_code: "",
    name: "",
    type: "",
    diskripsi: "",
    price: "",
  };

  const onSubmit = (data) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    const raw = `{
              "token":"SemogaDilancarkanSemuanayaAamiiin",
              "cmd":"IudDataTindakan",
              "func":"update",
              "id": "${idTindakan}",
              "clinic_code":"${addActionData.clinic_code}",
              "name":"${addActionData.name}",
              "type":"${addActionData.type}",
              "description":"${addActionData.diskripsi}",
              "price":"${addActionData.price}"
            }`;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);
        console.log(res);

        if (res.error === "0") {
          setIsSuccess(true);
          setMsg(res.setvar.varvalue);
          setTimeout(() => {
            history.push("/tindakan");
          }, 2000);
        } else {
          setIsSuccess(true);
          setMsg(res.setvar.varvalue);
        }
      })
      .catch((error) => {
        console.log("error", error);
        setIsErr(true);
        setMsg(error);
      });
  };

  const getCodeClinicAPI = () => {
    setIsLoaded(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw =
      '{"token":"SemogaDilancarkanSemuanayaAamiiin", "cmd":"LoadAllDataClinic", "cari":"", "posisi":0,"batas":100}';
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);

        if (res.error === 0) {
          let data = res.setvar.varvalue.map((item) => {
            return {
              code_clinic: item.code,
            };
          });

          setGetCodeKlinik(data);
        } else {
          console.log(res);
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  const getIdDataTindakan = () => {
    setIsLoaded(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw = `{"token":"SemogaDilancarkanSemuanayaAamiiin", "cmd":"LoadIdDataTindakan", "id": "${idTindakan}" }`;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);

        if (res.error === 0) {
          let data = res.setvar.varvalue;

          setKodeKlinik(data.clinic_code);
          setNamaTindakan(data.name);
          setTypeTindakan(data.type);
          setDiskripsi(data.description);
          setHarga(data.price);
        } else {
          console.log(res);
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  useEffect(() => {
    getCodeClinicAPI();
    getIdDataTindakan();
  }, []);

  useEffect(() => {
    addActionData.clinic_code = kodeKlinik;
  }, [addActionData, kodeKlinik]);

  useEffect(() => {
    addActionData.type = typeTindakan;
  }, [addActionData, typeTindakan]);

  useEffect(() => {
    addActionData.price = harga;
  }, [addActionData, harga]);

  useEffect(() => {
    addActionData.diskripsi = diskripsiTindakan;
  }, [addActionData, diskripsiTindakan]);

  useEffect(() => {
    addActionData.name = namaTindakan;
  }, [addActionData, namaTindakan]);

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const handleCloseSnack = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setIsSuccess(false);
    setIsErr(false);
  };

  const succcesMsg = (
    <Snackbar
      open={isSuccess}
      autoHideDuration={6000}
      onClose={handleCloseSnack}
    >
      <Alert onClose={handleCloseSnack} severity="success">
        {msg}
      </Alert>
    </Snackbar>
  );

  const errMsg = (
    <Snackbar open={isErr} autoHideDuration={6000} onClose={handleCloseSnack}>
      <Alert onClose={handleCloseSnack} severity="error">
        {msg}
      </Alert>
    </Snackbar>
  );

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Edit Data Tindakan
      </Typography>
      <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <TextField
              id="kodeKlinik"
              select
              fullWidth
              label="Kode Klinik"
              value={kodeKlinik}
              onChange={(e) => {
                setKodeKlinik(e.target.value);
              }}
              helperText="Pilih Kode Klinik"
            >
              {getCodeKlinik.map((option) => (
                <MenuItem key={option.code_clinic} value={option.code_clinic}>
                  {option.code_clinic}
                </MenuItem>
              ))}
            </TextField>
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="namaTindakan"
              label="Nama Tindakan"
              name="nama_tindakan"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setNamaTindakan(e.target.value)}
              value={namaTindakan}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="typeTindakan"
              label="Type Tindakan"
              name="type_tindakan"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setTypeTindakan(e.target.value)}
              value={typeTindakan}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="diskripsiTindakan"
              label="Diskripsi Tindakan"
              name="diskripsi_tindakan"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setDiskripsi(e.target.value)}
              value={diskripsiTindakan}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="harga"
              label="Harga "
              name="harga"
              fullWidth
              margin="normal"
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setHarga(e.target.value)}
              value={harga}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12}>
            <ButtonGroup
              color="primary"
              aria-label="outlined primary button group"
            >
              <Button
                onClick={() => {
                  history.push("/backend/tindakan");
                }}
              >
                Cancel
              </Button>
              <Button variant="contained" color="primary" type="submit">
                Save
              </Button>
            </ButtonGroup>
          </Grid>
        </Grid>
      </form>
      {succcesMsg}
      {errMsg}
    </React.Fragment>
  );
}
