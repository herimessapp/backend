import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";

import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { MenuItem } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function ClinicAdd() {
  const classes = useStyles();
  const history = useHistory();
  const [kodeKlinik, setKodeKlinik] = useState("");
  const [namaDokter, setNamaDokter] = useState("");
  const [getNamaDokter, setGetNamaDokter] = useState([]);

  const [getIdKlinik, setGetIdKlinik] = useState([]);
  const [getDataTindakan, setGetDataTindakan] = useState([]);
  const [tglJadwal, setTglJadwal] = useState("");
  const [tglRekam, setTglRekam] = useState("");
  const [waPasien, setWaPasien] = useState("");
  const [diagnosis, setDiagnosis] = useState("");
  const [keluhan, setKeluhan] = useState("");
  const [namaAkse, setNamaAksi] = useState("");
  const [isSuccess, setIsSuccess] = useState(false);
  const [isErr, setIsErr] = useState(false);
  const [msg, setMsg] = useState("");
  const [isLoaded, setIsLoaded] = useState(false);
  const { register, handleSubmit } = useForm();

  let addRekamMedis = {
    clinicId: "",
    clinic_code: "CN-BDG001",
    doctor_name: "Dr. Raihan",
    schedule_date: "2020-08-04",
    record_date: "2020-08-04",
    wa_patient: "6281312172280",
    diagnosis:
      "jenis kulit: normal, kelainan kulit: milia, lipatan dan garis kulit: kening, pori-pori: besar, Tonus: kencang",
    keluhan: "Kulit wajah ingin kencang",
    name_action: "Photo Facial",
  };

  const onSubmit = (data) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    console.log(addRekamMedis);
    var raw = `{
    "token":"SemogaDilancarkanSemuanayaAamiiin",
    "cmd":"IudDataRekamMedis",
    "func":"insert",
    "clinic_code":"${addRekamMedis.clinic_code}",
    "doctor_name":"${addRekamMedis.doctor_name}",
    "schedule_date":"${addRekamMedis.schedule_date}",
    "record_date":"${addRekamMedis.record_date}",
    "wa_patient":"${addRekamMedis.wa_patient}",
    "diagnosis":"${addRekamMedis.diagnosis}",
    "keluhan":"${addRekamMedis.keluhan}",
    "name_action":"${addRekamMedis.name_action}"
}`;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    console.log(raw);
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        console.log(result);
        let res = JSON.parse(result);
        console.log(res);

        if (res.error === "0") {
          setIsSuccess(true);
          setMsg(res.setvar.varvalue);
          setTimeout(() => {
            history.push("/rekam-medis");
          }, 2000);
        } else {
          setIsSuccess(true);
          setMsg(res.setvar.varvalue);
        }
      })
      .catch((error) => {
        console.log("error", error);
        setIsErr(true);
        setMsg(error);
      });
  };

  const getIdClinic = () => {
    setIsLoaded(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw =
      '{"token":"SemogaDilancarkanSemuanayaAamiiin", "cmd":"LoadAllDataClinic", "cari":"", "posisi":0,"batas":100}';
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);

        if (res.error === 0) {
          let data = res.setvar.varvalue.map((item) => {
            return {
              id_clinic: item.code,
              nama_clinic: item.name,
            };
          });

          setGetIdKlinik(data);
        } else {
          console.log(res);
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  const getActionData = (id) => {
    setIsLoaded(true);

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw = `{"token":"SemogaDilancarkanSemuanayaAamiiin", "cmd":"LoadAllDataTindakan", "cari":"", "posisi":0,"batas":100}`;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);

        if (res.error === 0) {
          let data = res.setvar.varvalue.map((item) => {
            return {
              nama_tindakan: item.name,
            };
          });

          setGetDataTindakan(data);
        } else {
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  const setNamaDokterAPI = (kode_klinik) => {
    setIsLoaded(true);

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw = `{"token":"SemogaDilancarkanSemuanayaAamiiin", "cmd":"LoadAllDataDokter", "cari":"${kode_klinik}", "posisi":0,"batas":100}`;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);

        if (res.error === 0) {
          let data = res.setvar.varvalue.map((item) => {
            return {
              nama_dokter: item.name,
              spesialis: item.specialist,
            };
          });

          setGetNamaDokter(data);
        } else {
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  useEffect(() => {
    getActionData();
    getIdClinic();
  }, []);

  useEffect(() => {
    addRekamMedis.clinic_code = kodeKlinik;
  }, [addRekamMedis, kodeKlinik]);

  useEffect(() => {
    addRekamMedis.doctor_name = namaDokter;
  }, [addRekamMedis, namaDokter]);

  useEffect(() => {
    addRekamMedis.schedule_date = tglJadwal;
  }, [addRekamMedis, tglJadwal]);

  useEffect(() => {
    addRekamMedis.record_date = tglRekam;
  }, [addRekamMedis, tglRekam]);

  useEffect(() => {
    addRekamMedis.wa_patient = waPasien;
  }, [addRekamMedis, waPasien]);

  useEffect(() => {
    addRekamMedis.diagnosis = diagnosis;
  }, [addRekamMedis, diagnosis]);

  useEffect(() => {
    addRekamMedis.keluhan = keluhan;
  }, [addRekamMedis, keluhan]);

  useEffect(() => {
    addRekamMedis.name_action = namaAkse;
  }, [addRekamMedis, namaAkse]);

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const handleCloseSnack = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setIsSuccess(false);
    setIsErr(false);
  };

  const succcesMsg = (
    <Snackbar
      open={isSuccess}
      autoHideDuration={6000}
      onClose={handleCloseSnack}
    >
      <Alert onClose={handleCloseSnack} severity="success">
        {msg}
      </Alert>
    </Snackbar>
  );

  const errMsg = (
    <Snackbar open={isErr} autoHideDuration={6000} onClose={handleCloseSnack}>
      <Alert onClose={handleCloseSnack} severity="error">
        {msg}
      </Alert>
    </Snackbar>
  );

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Tambah Data Rekam Medis
      </Typography>
      <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <TextField
              id="kodeKlinik"
              select
              fullWidth
              label="Kode Klinik"
              value={kodeKlinik}
              onChange={(e) => {
                setKodeKlinik(e.target.value);
                setNamaDokterAPI(e.target.value);
              }}
              helperText="Pilih Kode Klinik"
            >
              {getIdKlinik.map((option) => (
                <MenuItem key={option.id_clinic} value={option.id_clinic}>
                  {option.id_clinic} ( {option.nama_clinic} )
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              id="NamaDokter"
              select
              fullWidth
              label="Nama Dokter"
              value={namaDokter}
              onChange={(e) => {
                setNamaDokter(e.target.value);
              }}
              helperText="Pilih Nama Dokter, pastikan telah memilih kode klinik terlebih dahulu"
            >
              {getNamaDokter.map((option) => (
                <MenuItem key={option.specialist} value={option.nama_dokter}>
                  {option.nama_dokter} - Spesialis {option.spesialis}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              id="tglJadwal"
              type="text"
              fullWidth
              type="date"
              label="Tanggal Jadwal"
              InputLabelProps={{
                shrink: true,
              }}
              value={tglJadwal}
              onChange={(e) => setTglJadwal(e.target.value)}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="tglRekam"
              label="Tanggal Rekam"
              name="tanggal_rekam"
              fullWidth
              type="date"
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setTglRekam(e.target.value)}
              value={tglRekam}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              id="waPasien"
              fullWidth
              label="Wa Pasien"
              value={waPasien}
              type="number"
              onChange={(e) => setWaPasien(e.target.value)}
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="diagnosis"
              label="Diagnosis"
              name="diagnosis"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setDiagnosis(e.target.value)}
              value={diagnosis}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="keluhan"
              label="Keluhan"
              name="keluhan"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setKeluhan(e.target.value)}
              value={keluhan}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              id="namaAksi"
              select
              fullWidth
              label="Nama Aksi"
              value={namaAkse}
              onChange={(e) => {
                setNamaAksi(e.target.value);
              }}
              helperText="Pilih Nama Aksi"
            >
              {getDataTindakan.map((option) => (
                <MenuItem
                  key={option.nama_tindakan}
                  value={option.nama_tindakan}
                >
                  {option.nama_tindakan}
                </MenuItem>
              ))}
            </TextField>
          </Grid>

          <Grid item xs={12}>
            <ButtonGroup
              color="primary"
              aria-label="outlined primary button group"
            >
              <Button
                onClick={() => {
                  history.push("/backend/rekam-medis");
                }}
              >
                Cancel
              </Button>
              <Button variant="contained" color="primary" type="submit">
                Save
              </Button>
            </ButtonGroup>
          </Grid>
        </Grid>
      </form>
      {succcesMsg}
      {errMsg}
    </React.Fragment>
  );
}
