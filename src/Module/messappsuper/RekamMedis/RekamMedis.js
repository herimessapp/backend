import React, { useState, useEffect, forwardRef } from "react";
import { useHistory } from "react-router-dom";
import Snackbar from "@material-ui/core/Snackbar";

import { makeStyles } from "@material-ui/core/styles";
import MuiAlert from "@material-ui/lab/Alert";
import Grid from "@material-ui/core/Grid";
import Title from "../Title";
import Button from "@material-ui/core/Button";

import MaterialTable from "material-table";
import {
  AddBox,
  ArrowDownward,
  Check,
  Clear,
  DeleteOutline,
  ChevronRight,
  Edit,
  SaveAlt,
  FilterList,
  FirstPage,
  LastPage,
  ChevronLeft,
  Search,
  Remove,
  ViewColumn,
} from "@material-ui/icons";

import IconButton from "@material-ui/core/IconButton";

function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function Doctor() {
  const history = useHistory();
  const [rows, setRows] = useState([]);
  const [error] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const [isErr, setIsErr] = useState(false);
  const [msg, setMsg] = useState("");

  const getRows = () => {
    setRows([]);
    setIsLoaded(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw = `
        {
          "token":"SemogaDilancarkanSemuanayaAamiiin",
          "cmd":"LoadAllDataRekamMedis",
          "clinic_code":"CN-BDG001",
          "cari":"",
          "posisi":0,
          "batas":100
        }
      `;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);
        if (res.error === 0) {
          let data = res.setvar.varvalue.map((item) => {
            return {
              id: item.id,
              antrian: item.antrian,
              invoice_number: item.invoice_number,
              clinic_code: item.clinic_code,
              doctor_name: item.doctor_name,
              record_date: item.record_date,
              wa_patient: item.wa_patient,
              biometrik: item.biometrik,
              patient_name: item.patient_name,
              diagnosis: item.diagnosis,
              keluhan: item.keluhan,
              name_action: item.name_action,
            };
          });
          console.log(data);
          setRows(data);
        } else {
          console.log(res);
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  const alarm = () => {
    setInterval(() => {
      document.body.backgroundColor = "red";
      document.body.innerHTML = "Waktu Hampir Selesai!!!";
    }, 1000);
    console.log(rows);
    rows.forEach((data) => {
      let antrian = data.antirian;
      console.log(antrian);
    });
  };

  useEffect(() => {
    getRows();
  }, []);

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const handleCloseSnack = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setIsSuccess(false);
    setIsErr(false);
  };

  const succcesMsg = (
    <Snackbar
      open={isSuccess}
      autoHideDuration={6000}
      onClose={handleCloseSnack}
    >
      <Alert onClose={handleCloseSnack} severity="success">
        {msg}
      </Alert>
    </Snackbar>
  );

  const errMsg = (
    <Snackbar open={isErr} autoHideDuration={6000} onClose={handleCloseSnack}>
      <Alert onClose={handleCloseSnack} severity="error">
        {msg}
      </Alert>
    </Snackbar>
  );

  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {
    return (
      <React.Fragment>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <Title>Data Rekam Medis</Title>
            <Button
              variant="contained"
              color="primary"
              size="small"
              style={{ marginBottom: "10px" }}
              endIcon={<i class="ri-add-fill"></i>}
              onClick={() => {
                history.push("/backend/rekam-medis/add");
              }}
            >
              Tambah
            </Button>
          </Grid>
        </Grid>
        <MaterialTable
          icons={tableIcons}
          options={{
            paging: true,
            sorting: true,
            headerStyle: {
              backgroundColor: "#01579b",
              color: "#FFF",
            },
          }}
          title=""
          columns={[
            { title: "Antrian", field: "antrian" },
            { title: "Invoice Number ", field: "invoice_number" },
            { title: "Kode Klinik", field: "clinic_code" },
            { title: "Nama Dokter", field: "doctor_name" },
            { title: "Tanggal Record", field: "record_date" },
            { title: "Wa Pasien", field: "wa_patient" },
            { title: "Biometrik", field: "biometrik" },
            { title: "Nama Pasien", field: "patient_name" },
            { title: "Diagnosis", field: "diagnosis" },
            { title: "Keluhan", field: "keluhan" },
            { title: "Nama Tindakan", field: "name_action" },
            {
              field: "id",
              title: "Action",
              cellStyle: {
                textAlign: "right",
              },
              headerStyle: {
                textAlign: "right",
              },
              render: (rowData) => (
                <>
                  <IconButton
                    color="secondary"
                    aria-label="add an alarm"
                    onClick={() => {
                      var myHeaders = new Headers();
                      myHeaders.append("Content-Type", "text/plain");
                      var raw =
                        '{"token":"SemogaDilancarkanSemuanayaAamiiin", "cmd":"IudDataRekamMedis", "func" : "delete", ' +
                        '"id":"' +
                        rowData.id +
                        '"}';
                      var requestOptions = {
                        method: "POST",
                        headers: myHeaders,
                        body: raw,
                        redirect: "follow",
                      };

                      fetch(
                        "https://clinic.messapp.id/v2/index.php",
                        requestOptions
                      )
                        .then((response) => response.text())
                        .then((result) => {
                          console.log(raw);
                          console.log(result);
                          let res = JSON.parse(result);
                          if (res.error === "0") {
                            setIsSuccess(true);
                            setMsg(res.setvar.varvalue);
                            getRows();
                          } else {
                            setIsSuccess(true);
                            setMsg(res.setvar.varvalue);
                          }
                        })
                        .catch((error) => console.log("error", error));
                    }}
                  >
                    <i class="ri-delete-bin-line"></i>
                  </IconButton>

                  <IconButton
                    color="default"
                    aria-label=""
                    onClick={() => {
                      history.push(`/backend/rekam-medis/edit/${rowData.id}`);
                    }}
                  >
                    <i class="ri-edit-box-line"></i>
                  </IconButton>
                </>
              ),
            },
          ]}
          data={rows}
        />
        {succcesMsg}
        {errMsg}
      </React.Fragment>
    );
  }
}
