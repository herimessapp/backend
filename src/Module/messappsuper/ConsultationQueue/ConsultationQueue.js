import React, { useState, useEffect } from "react";
import Link from "@material-ui/core/Link";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Title from "../Title";
import IconButton from "@material-ui/core/IconButton";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import SearchBar from "material-ui-search-bar";
import Pagination from "@material-ui/lab/Pagination";
import { useHistory } from "react-router-dom";

function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function Consultation() {
  const classes = useStyles();

  const history = useHistory();
  const theme = useTheme();
  const [rows, setRows] = useState([]);
  const [error] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");

    var raw =
      '{\n   "token":"SemogaDilancarkanSemuanayaAamiiin",\n   "cmd":"GetOperator",\n   "cari"      : "",\n   "posisi" : 0,\n   "batas"     : 10\n}';

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://wajek.messapp.id/service/v1/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);
        if (res.error === 0) {
          setRows(res.setvar.varvalue);
        } else {
          console.log(res);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  }, []);

  // if (error) {
  //   return <div>Error: {error.message}</div>;
  // } else if (!isLoaded) {
  //   return <div>Loading...</div>;
  // } else {
  return (
    <React.Fragment>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <Title>Data Antrian Konsultasi</Title>
          <Button
            variant="contained"
            color="primary"
            size="small"
            className={classes.button}
            style={{ marginBottom: "10px" }}
            endIcon={<i class="ri-send-plane-2-fill"></i>}
            onClick={() => {
              history.push("/backend/antrian/add");
            }}
          >
            Tambah
          </Button>
        </Grid>
        <Grid item xs={12} sm={6}>
          <SearchBar />
        </Grid>
      </Grid>
      <Table size="small" stickyHeader aria-label="sticky table" dense table>
        <TableHead>
          <TableRow>
            <TableCell>Nama Klinik</TableCell>
            <TableCell>Nomer Antrian</TableCell>
            <TableCell>Nama Pasien</TableCell>
            <TableCell>Nama Dokter</TableCell>
            <TableCell>Jadwal Dokter</TableCell>
            <TableCell>Keluhan Pasien</TableCell>
            <TableCell>Status</TableCell>
            <TableCell align="right">Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow>
            <TableCell>Data</TableCell>
            <TableCell>Data</TableCell>
            <TableCell>Data</TableCell>
            <TableCell>Data</TableCell>
            <TableCell>Data</TableCell>
            <TableCell>Data</TableCell>
            <TableCell>Data</TableCell>

            <TableCell align="right">
              <IconButton color="secondary" aria-label="add an alarm">
                <i class="ri-delete-bin-fill"></i>
              </IconButton>
              <IconButton color="action" aria-label="add an alarm">
                <i class="ri-edit-box-fill"></i>
              </IconButton>
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          padding: "10px 0",
          paddingTop: "20px",
        }}
      >
        <Pagination count={10} color="primary" />
      </div>
    </React.Fragment>
  );
}
