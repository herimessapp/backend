import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";

import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function ConsultationQueueAdd() {
  const classes = useStyles();
  const history = useHistory();
  const [namaKlinik, setNamaKlinik] = useState("");
  const [typeKonsultasi, setTypeKonsultasi] = useState("");
  const [harga, setHarga] = useState("");
  const { register, handleSubmit } = useForm();
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Tambah Data Konsultasi
      </Typography>
      <form className={classes.form}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="namaKlinik"
              label="Nama Klinik"
              name="nama_klinik"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setNamaKlinik(e.target.value)}
              value={namaKlinik}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="typeKonsultasi"
              label="Type Konsultasi"
              name="type_konsultasi"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setTypeKonsultasi(e.target.value)}
              value={typeKonsultasi}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="harga"
              label="Harga Konsultasi"
              name="harga_konsultasi"
              fullWidth
              margin="normal"
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setHarga(e.target.value)}
              value={harga}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12}>
            <ButtonGroup
              color="primary"
              aria-label="outlined primary button group"
            >
              <Button
                onClick={() => {
                  history.push("/backend/operator");
                }}
              >
                Cancel
              </Button>
              <Button variant="contained" color="primary" type="submit">
                Save
              </Button>
            </ButtonGroup>
          </Grid>
        </Grid>
      </form>
    </React.Fragment>
  );
}
