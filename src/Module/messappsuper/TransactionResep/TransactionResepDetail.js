import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router-dom";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

export default function SimpleTable() {
  const [error] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const [isErr, setIsErr] = useState(false);
  const [msg, setMsg] = useState("");
  const { codeBasket } = useParams();
  const history = useHistory();

  const [rows, setRows] = useState([]);
  const [barang, setBarang] = useState([]);

  const getRows = () => {
    setIsLoaded(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw = `
        {
    "token": "SemogaDilancarkanSemuanayaAamiiin",
    "cmd": "DetailTrxResepDanObat",
    "clinic_code": "CN-BDG001",
    "code_basket": "${codeBasket}"
}
      `;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);
        if (res.error === 0) {
          let data = res.setvar;
          data.pop();
          setRows(data);
        } else {
          console.log(res);
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  const getDataBarang = () => {
    setIsLoaded(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw = `
        {
    "token": "SemogaDilancarkanSemuanayaAamiiin",
    "cmd": "DetailTrxResepDanObat",
    "clinic_code": "CN-BDG001",
    "code_basket": "${codeBasket}"
}
      `;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);
        if (res.error === 0) {
          let barang = res.setvar;
          barang.splice(0, 1);
          barang.splice(1, 1);
          barang.splice(2, 1);
          barang.splice(3, 1);
          barang.splice(4, 1);
          barang.splice(5, 1);
          barang.splice(6, 1);
          barang.shift(1);
          barang.shift(2);
          barang.shift(3);
          barang.shift(4);
          setBarang(barang);
        } else {
          console.log(res);
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  console.log(barang);

  useEffect(() => {
    getRows();
    getDataBarang();
  }, []);

  const classes = useStyles();

  return (
    <>
      <TableContainer component={Paper}>
        <Typography style={{ paddingBottom: "10px" }}>
          Detail Transaksi Resep dan Obat
        </Typography>
        <Table className={classes.table} aria-label="simple table">
          <TableBody>
            {rows.map((row, index) => {
              return (
                <TableRow key={index}>
                  <TableCell component="th" scope="row">
                    {row.varname} :
                  </TableCell>
                  <TableCell component="th" scope="row">
                    {row.varvalue}
                  </TableCell>
                </TableRow>
              );
            })}
            {barang.map((item, index) => {
              return (
                <>
                  <TableRow key={index}>
                    <TableCell component="th" scope="row">
                      Det Shop
                    </TableCell>
                    {item.varvalue.map((shop) => {
                      return <div>{shop.det_shop}</div>;
                    })}
                  </TableRow>
                  <TableRow key={index}>
                    <TableCell component="th" scope="row">
                      Nama Barang
                    </TableCell>
                    {item.varvalue.map((item) => {
                      return <div>{item.nama_barang}</div>;
                    })}
                  </TableRow>
                  <TableRow key={index}>
                    <TableCell component="th" scope="row">
                      Harga Barang
                    </TableCell>
                    {item.varvalue.map((item) => {
                      return <div>{item.harga}</div>;
                    })}
                  </TableRow>
                  <TableRow key={index}>
                    <TableCell component="th" scope="row">
                      Qty Barang
                    </TableCell>
                    {item.varvalue.map((item) => {
                      return <div>{item.qyt}</div>;
                    })}
                  </TableRow>
                  <TableRow key={index}>
                    <TableCell component="th" scope="row">
                      Unit Barang
                    </TableCell>
                    {item.varvalue.map((item) => {
                      return <div>{item.unit}</div>;
                    })}
                  </TableRow>
                  <TableRow key={index}>
                    <TableCell component="th" scope="row">
                      Total Barang
                    </TableCell>
                    {item.varvalue.map((item) => {
                      return <div>{item.total}</div>;
                    })}
                  </TableRow>
                  <TableRow key={index}>
                    <TableCell component="th" scope="row">
                      Berat Barang
                    </TableCell>
                    {item.varvalue.map((item) => {
                      return <div>{item.berat}</div>;
                    })}
                  </TableRow>
                </>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <Button
        variant="contained"
        color="primary"
        size="small"
        style={{ marginBottom: "10px" }}
        endIcon={<i class="ri-edit-box-line"></i>}
        onClick={() =>
          history.push(`/backend/transaksi-resep/waybill/${codeBasket}`)
        }
      >
        Update Waybill
      </Button>
    </>
  );
}
