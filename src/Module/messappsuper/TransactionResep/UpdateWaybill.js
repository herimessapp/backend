import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";

import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { useParams } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function ClinicAdd() {
  const classes = useStyles();
  const history = useHistory();
  const [kodeKlinik, setKodeKlinik] = useState("");
  const [getCodeKlinik, setGetCodeKlinik] = useState([]);
  const [waybill, setWaybill] = useState("");

  const [isSuccess, setIsSuccess] = useState(false);
  const [isErr, setIsErr] = useState(false);
  const [msg, setMsg] = useState("");
  const [isLoaded, setIsLoaded] = useState(false);
  const { register, handleSubmit } = useForm();
  const { codeBasket } = useParams();

  let updateWaybill = {
    waybill: "12231231",
  };

  const onSubmit = (data) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw = `{
        "token": "SemogaDilancarkanSemuanayaAamiiin",
        "cmd": "getUpdateWayBill",
        "clinic_code": "CN-BDG001",
        "code_basket": "${codeBasket}",
        "waybill": "${updateWaybill.waybill}"
    }`;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    console.log(raw);
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        console.log(result);
        let res = JSON.parse(result);
        console.log(res);

        if (res.error === "0") {
          setIsSuccess(true);
          setMsg(res.setvar.varvalue);
          setTimeout(() => {
            history.push(`/backend/transaksi-resep/detail/${codeBasket}`);
          }, 2000);
        } else {
          setIsSuccess(true);
          setMsg(res.setvar.varvalue);
        }
      })
      .catch((error) => {
        console.log("error", error);
        setIsErr(true);
        setMsg(error);
      });
  };

  const GetWayBill = () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw = `{
    "token": "SemogaDilancarkanSemuanayaAamiiin",
    "cmd": "DetailTrxResepDanObat",
    "clinic_code": "CN-BDG001",
    "code_basket": "${codeBasket}"
}`;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    console.log(raw);
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);
        if (res.error === 0) {
          let barang = res.setvar[0];
        } else {
          console.log(res);
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => {
        console.log("error", error);
        setIsErr(true);
        setMsg(error);
      });
  };

  useEffect(() => {
    GetWayBill();
  });

  useEffect(() => {
    updateWaybill.clinic_code = kodeKlinik;
  }, [updateWaybill, kodeKlinik]);

  useEffect(() => {
    updateWaybill.waybill = waybill;
  }, [updateWaybill, waybill]);

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const handleCloseSnack = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setIsSuccess(false);
    setIsErr(false);
  };

  const succcesMsg = (
    <Snackbar
      open={isSuccess}
      autoHideDuration={6000}
      onClose={handleCloseSnack}
    >
      <Alert onClose={handleCloseSnack} severity="success">
        {msg}
      </Alert>
    </Snackbar>
  );

  const errMsg = (
    <Snackbar open={isErr} autoHideDuration={6000} onClose={handleCloseSnack}>
      <Alert onClose={handleCloseSnack} severity="error">
        {msg}
      </Alert>
    </Snackbar>
  );

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Edit Waybill
      </Typography>
      <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <TextField
              id="waybill"
              fullWidth
              label="Waybill"
              value={waybill}
              onChange={(e) => {
                setWaybill(e.target.value);
              }}
            ></TextField>
          </Grid>

          <Grid item xs={12}>
            <ButtonGroup
              color="primary"
              aria-label="outlined primary button group"
            >
              <Button
                onClick={() => {
                  history.push(`/backend/transaksi-resep/detail/${codeBasket}`);
                }}
              >
                Cancel
              </Button>
              <Button variant="contained" color="primary" type="submit">
                Save
              </Button>
            </ButtonGroup>
          </Grid>
        </Grid>
      </form>
      {succcesMsg}
      {errMsg}
    </React.Fragment>
  );
}
