import React from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Chart from "./Chart";
// import Deposits from "./Deposits";
// import Orders from "./Orders";
import Copyright from "../../component/Copyright";
import { Switch, Route } from "react-router-dom";
import DrawerSuper from "./DrawerSuper";
import ApplicationBar from "../../component/ApplicationBar";
import User from "./User/User";
import UserAdd from "./User/UserAdd";
import UserEdit from "./User/UserEdit";
import Clinic from "./Clinic/Clinic";
import ClinicAdd from "./Clinic/ClinicAdd";
import ClinicEdit from "./Clinic/ClinicEdit";
import Doctor from "./Doctor/Doctor";
import DoctorAdd from "./Doctor/DoctorAdd";
import DoctorEdit from "./Doctor/DoctorEdit";
import Patient from "./Patient/Patient";
import PatientAdd from "./Patient/PatientAdd";
import PatientEdit from "./Patient/PatientEdit";
import Consultation from "./Consultation/Consultation";
import ConsultationAdd from "./Consultation/ConsultationAdd";
import ConsultationEdit from "./Consultation/ConsultationEdit";
import SettingQueue from "./SettingQueue/SettingQueue";
import SettingQueueAdd from "./SettingQueue/SettingQueueAdd";
import SettingQueueEdit from "./SettingQueue/SettingQueueEdit";
import Action from "./Action/Action";
import ActionAdd from "./Action/ActionAdd";
import ActionEdit from "./Action/ActionEdit";
import ConsultationQueue from "./ConsultationQueue/ConsultationQueue";
import ConsultationQueueAdd from "./ConsultationQueue/ConsultationQueueAdd";
import Medicine from "./Medicine/Medicine";
import MedicineAdd from "./Medicine/MedicineAdd";
import MedicineEdit from "./Medicine/MedicineEdit";
import RekamMedis from "./RekamMedis/RekamMedis";
import RekamMedisAdd from "./RekamMedis/RekamMedisAdd";
import RekamMedisEdit from "./RekamMedis/RekamMedisEdit";
import Resep from "./Resep/Resep";
import ResepAdd from "./Resep/ResepAdd";
import ResepEdit from "./Resep/ResepEdit";
import TransaksiResep from "./TransactionResep/TransactionResep";
import TransaksiResepDetail from "./TransactionResep/TransactionResepDetail";
import TransaksiResepEdit from "./TransactionResep/UpdateWaybill";
import TransaksiPerawatan from "./TransaksiPerawatan/TransaksiPerawatan";
import TransaksiKonsultasi from "./TransaksiKonsultasi/TransaksiKonsultasi";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: "none",
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
}));

export default function DashboardSuper() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  return (
    <Switch>
      <Route path="/backend/user/edit/:id">
        <UserEdit open={open} setOpen={setOpen} />
      </Route>
      <Route path="/backend/user/add">
        <UserAdd open={open} setOpen={setOpen} />
      </Route>
      <Route path="/backend/user">
        <User open={open} setOpen={setOpen} />
      </Route>
      <Route path="/backend/user-otoritas">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <User />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
      <Route exact path="/backend/klinik">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <Clinic />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
      <Route path="/backend/klinik/add">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <ClinicAdd />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
      <Route path="/backend/klinik/edit/:idClinic">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <ClinicEdit />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
      <Route exact path="/backend/dokter">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <Doctor />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
      <Route path="/backend/dokter/add">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <DoctorAdd />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route path="/backend/dokter/edit/:idDokter">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <DoctorEdit />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route exact path="/backend/pasien">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <Patient />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route path="/backend/pasien/add">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <PatientAdd />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route path="/backend/pasien/edit/:idPasien">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <PatientEdit />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route exact path="/backend/konsultasi">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <Consultation />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
      <Route path="/backend/konsultasi/add">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <ConsultationAdd />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route path="/backend/konsultasi/edit/:idKonsultasi">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <ConsultationEdit />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route exact path="/backend/setting-antrian/">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <SettingQueue />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route path="/backend/setting-antrian/add">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <SettingQueueAdd />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route path="/backend/setting-antrian/edit/:idSettingAntrian">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <SettingQueueEdit />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route exact path="/backend/tindakan">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <Action />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
      <Route path="/backend/tindakan/add">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <ActionAdd />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route path="/backend/tindakan/edit/:idTindakan">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <ActionEdit />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route exact path="/backend/antrian">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <ConsultationQueue />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route exact path="/backend/antrian/add">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <ConsultationQueueAdd />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route exact path="/backend/obat">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <Medicine />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route path="/backend/obat/add">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <MedicineAdd />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route path="/backend/obat/edit/:idObat">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <MedicineEdit />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route exact path="/backend/rekam-medis">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <RekamMedis />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route path="/backend/rekam-medis/add">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <RekamMedisAdd />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route path="/backend/rekam-medis/edit/:idRekamMedis">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <RekamMedisEdit />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route exact path="/backend/resep">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <Resep />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route path="/backend/resep/add">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <ResepAdd />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route path="/backend/resep/edit/:idResep">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <ResepEdit />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route exact path="/backend/transaksi-resep">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <TransaksiResep />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route path="/backend/transaksi-resep/detail/:codeBasket">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <TransaksiResepDetail />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route path="/backend/transaksi-resep/waybill/:codeBasket">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <TransaksiResepEdit />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route exact path="/backend/transaksi-perawatan">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <TransaksiPerawatan />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route exact path="/backend/transaksi-konsultasi">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <TransaksiKonsultasi />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>

      <Route path="/backend">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerSuper open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                {/* Chart */}
                <Grid item xs={12} md={12} lg={12}>
                  <Paper style={{ height: "80vh", padding: "2em" }}>
                    <Chart />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
    </Switch>
  );
}
