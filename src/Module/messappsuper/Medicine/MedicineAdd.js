import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";

import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { MenuItem } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function ActionAdd() {
  const classes = useStyles();
  const history = useHistory();
  const [kodeKlinik, setKodeKlinik] = useState("");
  const [kodeObat, setKodeObat] = useState("");
  const [getCodeKlinik, setGetCodeKlinik] = useState([]);
  const [namaObat, setNamaObat] = useState("");
  const [typeObat, setTypeObat] = useState("");
  const [supplier, setSupplier] = useState("");
  const [harga, setHarga] = useState("");
  const [unit, setUnit] = useState("");
  const [stok, setStok] = useState("");
  const [isSuccess, setIsSuccess] = useState(false);
  const [isErr, setIsErr] = useState(false);
  const [msg, setMsg] = useState("");
  const [isLoaded, setIsLoaded] = useState(false);

  const { register, handleSubmit } = useForm();

  let addDataMedicine = {
    clinic_code: "",
    code: "",
    name: "",
    type: "",
    supplier: "",
    price: "",
    unit: "",
    stok: "",
  };

  const onSubmit = (data) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    const raw = `{
              "token":"SemogaDilancarkanSemuanayaAamiiin",
              "cmd":"IudDataObat",
              "func":"insert",
              "code": "${addDataMedicine.code}",
              "clinic_code":"${addDataMedicine.clinic_code}",
              "name":"${addDataMedicine.name}",
              "type":"${addDataMedicine.type}",
              "supplier":"${addDataMedicine.supplier}",
              "price":"${addDataMedicine.price}",
              "unit":"${addDataMedicine.unit}",
              "stok":"${addDataMedicine.stok}"
            }`;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);
        console.log(res);

        if (res.error === "0") {
          setIsSuccess(true);
          setMsg(res.setvar.varvalue);
          setTimeout(() => {
            history.push("/obat");
          }, 2000);
        } else {
          setIsSuccess(true);
          setMsg(res.setvar.varvalue);
        }
      })
      .catch((error) => {
        console.log("error", error);
        setIsErr(true);
        setMsg(error);
      });
  };

  const getCodeClinicAPI = () => {
    setIsLoaded(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw =
      '{"token":"SemogaDilancarkanSemuanayaAamiiin", "cmd":"LoadAllDataClinic", "cari":"", "posisi":0,"batas":100}';
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);

        if (res.error === 0) {
          let data = res.setvar.varvalue.map((item) => {
            return {
              code_clinic: item.code,
              nama_clinic: item.name,
            };
          });

          setGetCodeKlinik(data);
        } else {
          console.log(res);
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  useEffect(() => {
    getCodeClinicAPI();
  }, []);

  useEffect(() => {
    addDataMedicine.code = kodeObat;
  }, [addDataMedicine, kodeObat]);

  useEffect(() => {
    addDataMedicine.clinic_code = kodeKlinik;
  }, [addDataMedicine, kodeKlinik]);

  useEffect(() => {
    addDataMedicine.type = typeObat;
  }, [addDataMedicine, typeObat]);

  useEffect(() => {
    addDataMedicine.price = harga;
  }, [addDataMedicine, harga]);

  useEffect(() => {
    addDataMedicine.name = namaObat;
  }, [addDataMedicine, namaObat]);

  useEffect(() => {
    addDataMedicine.supplier = supplier;
  }, [addDataMedicine, supplier]);

  useEffect(() => {
    addDataMedicine.unit = unit;
  }, [addDataMedicine, unit]);

  useEffect(() => {
    addDataMedicine.stok = stok;
  }, [addDataMedicine, stok]);

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const handleCloseSnack = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setIsSuccess(false);
    setIsErr(false);
  };

  const succcesMsg = (
    <Snackbar
      open={isSuccess}
      autoHideDuration={6000}
      onClose={handleCloseSnack}
    >
      <Alert onClose={handleCloseSnack} severity="success">
        {msg}
      </Alert>
    </Snackbar>
  );

  const errMsg = (
    <Snackbar open={isErr} autoHideDuration={6000} onClose={handleCloseSnack}>
      <Alert onClose={handleCloseSnack} severity="error">
        {msg}
      </Alert>
    </Snackbar>
  );

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Tambah Data Obat
      </Typography>
      <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <TextField
              id="kodeKlinik"
              select
              fullWidth
              label="Kode Klinik"
              value={kodeKlinik}
              onChange={(e) => {
                setKodeKlinik(e.target.value);
              }}
              helperText="Pilih Kode Klinik"
            >
              {getCodeKlinik.map((option) => (
                <MenuItem key={option.code_clinic} value={option.code_clinic}>
                  {option.code_clinic} ( {option.nama_clinic})
                </MenuItem>
              ))}
            </TextField>
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="kodeObat"
              label="Kode Obat"
              name="kode_obat"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setKodeObat(e.target.value)}
              value={kodeObat}
              type="number"
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="namaObat"
              label="Nama Obat"
              name="nama_obat"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setNamaObat(e.target.value)}
              value={namaObat}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="typeObat"
              label="Type Obat"
              name="type_obat"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setTypeObat(e.target.value)}
              value={typeObat}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="supplier"
              label="Supplier"
              name="supplier"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setSupplier(e.target.value)}
              value={supplier}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="harga"
              label="Harga "
              name="harga"
              fullWidth
              margin="normal"
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setHarga(e.target.value)}
              value={harga}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="unit"
              label="unit "
              name="unit"
              fullWidth
              margin="normal"
              type="text"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setUnit(e.target.value)}
              value={unit}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="stok"
              label="stok "
              name="stok"
              fullWidth
              margin="normal"
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setStok(e.target.value)}
              value={stok}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12}>
            <ButtonGroup
              color="primary"
              aria-label="outlined primary button group"
            >
              <Button
                onClick={() => {
                  history.push("/backend/obat");
                }}
              >
                Cancel
              </Button>
              <Button variant="contained" color="primary" type="submit">
                Save
              </Button>
            </ButtonGroup>
          </Grid>
        </Grid>
      </form>
      {succcesMsg}
      {errMsg}
    </React.Fragment>
  );
}
