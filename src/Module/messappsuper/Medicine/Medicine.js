import React, { useState, useEffect, forwardRef } from "react";
import { useHistory } from "react-router-dom";
import Snackbar from "@material-ui/core/Snackbar";

import { makeStyles } from "@material-ui/core/styles";
import MuiAlert from "@material-ui/lab/Alert";
import Grid from "@material-ui/core/Grid";
import Title from "../Title";
import Button from "@material-ui/core/Button";

import MaterialTable from "material-table";
import {
  AddBox,
  ArrowDownward,
  Check,
  Clear,
  DeleteOutline,
  ChevronRight,
  Edit,
  SaveAlt,
  FilterList,
  FirstPage,
  LastPage,
  ChevronLeft,
  Search,
  Remove,
  ViewColumn,
} from "@material-ui/icons";

import IconButton from "@material-ui/core/IconButton";

function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function Medicine() {
  const history = useHistory();
  const [rows, setRows] = useState([]);
  const [error] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const [isErr, setIsErr] = useState(false);
  const [msg, setMsg] = useState("");

  const getRows = () => {
    setRows([]);
    setIsLoaded(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw = `{"token":"SemogaDilancarkanSemuanayaAamiiin", "cmd":"LoadAllDataObat", "cari":"", "posisi":0,"batas":100}`;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);
        if (res.error === 0) {
          let data = res.setvar.varvalue.map((item) => {
            return {
              id: item.id,
              code: item.code,
              clinic_code: item.clinic_code,
              name: item.name,
              type: item.type,
              supplier: item.supplier,
              price: item.price,
              unit: item.unit,
              stok: item.stok,
            };
          });
          setRows(data);
        } else {
          console.log(res);
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  useEffect(() => {
    getRows();
  }, []);

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const handleCloseSnack = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setIsSuccess(false);
    setIsErr(false);
  };

  const succcesMsg = (
    <Snackbar
      open={isSuccess}
      autoHideDuration={6000}
      onClose={handleCloseSnack}
    >
      <Alert onClose={handleCloseSnack} severity="success">
        {msg}
      </Alert>
    </Snackbar>
  );

  const errMsg = (
    <Snackbar open={isErr} autoHideDuration={6000} onClose={handleCloseSnack}>
      <Alert onClose={handleCloseSnack} severity="error">
        {msg}
      </Alert>
    </Snackbar>
  );

  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {
    return (
      <React.Fragment>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <Title>Data Obat</Title>
            <Button
              variant="contained"
              color="primary"
              size="small"
              style={{ marginBottom: "10px" }}
              endIcon={<i class="ri-add-fill"></i>}
              onClick={() => {
                history.push("/backend/obat/add");
              }}
            >
              Tambah
            </Button>
          </Grid>
        </Grid>
        <MaterialTable
          icons={tableIcons}
          options={{
            paging: true,
            sorting: true,
            headerStyle: {
              backgroundColor: "#01579b",
              color: "#FFF",
            },
          }}
          title=""
          columns={[
            { title: "Kode", field: "code" },
            { title: "Kode Klinik", field: "clinic_code" },
            { title: "Nama", field: "name" },
            { title: "Type", field: "type" },
            { title: "Supplier", field: "supplier" },
            { title: "Harga", field: "price" },
            { title: "Unit", field: "unit" },
            { title: "Stok", field: "stok" },
            {
              field: "id",
              title: "Action",
              cellStyle: {
                textAlign: "right",
              },
              headerStyle: {
                textAlign: "right",
              },
              render: (rowData) => (
                <>
                  <IconButton
                    color="secondary"
                    aria-label="add an alarm"
                    onClick={() => {
                      var myHeaders = new Headers();
                      myHeaders.append("Content-Type", "text/plain");
                      var raw =
                        '{"token":"SemogaDilancarkanSemuanayaAamiiin", "cmd" : "IudDataObat", "func" : "delete", ' +
                        '"id":"' +
                        rowData.id +
                        '"}';
                      var requestOptions = {
                        method: "POST",
                        headers: myHeaders,
                        body: raw,
                        redirect: "follow",
                      };

                      fetch(
                        "https://clinic.messapp.id/v2/index.php",
                        requestOptions
                      )
                        .then((response) => response.text())
                        .then((result) => {
                          console.log(result);
                          let res = JSON.parse(result);
                          getRows();
                          setMsg(res.setvar.varvalue);
                          setIsSuccess(true);
                        })
                        .catch((error) => console.log("error", error));
                    }}
                  >
                    <i class="ri-delete-bin-line"></i>
                  </IconButton>

                  <IconButton
                    color="default"
                    aria-label=""
                    onClick={() => {
                      history.push(`/backend/obat/edit/${rowData.id}`);
                    }}
                  >
                    <i class="ri-edit-box-line"></i>
                  </IconButton>
                </>
              ),
            },
          ]}
          data={rows}
        />
        {succcesMsg}
        {errMsg}
      </React.Fragment>
    );
  }
}
