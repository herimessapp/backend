import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  ResponsiveContainer,
} from "recharts";
import Grid from "@material-ui/core/Grid";
import { useForm } from "react-hook-form";
import TextField from "@material-ui/core/TextField";

import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { useHistory } from "react-router-dom";
import { MenuItem } from "@material-ui/core";

import Title from "./Title";

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function Chart() {
  const classes = useStyles();
  const history = useHistory();
  const [cmdTransaksi, setCmdTransaksi] = useState("");
  const [mulai, setMulai] = useState("");
  const [sampaiTgl, setSampaiTgl] = useState("");

  const [data, setData] = useState([]);
  const [isSuccess, setIsSuccess] = useState(false);
  const [isErr, setIsErr] = useState(false);
  const [msg, setMsg] = useState("");
  const [isLoaded, setIsLoaded] = useState(false);
  const { register, handleSubmit } = useForm();

  let addResepData = {
    clinic_code: "CN-BDG001",
    id_rekam_medis: "2",
    id_obat: 1,
    qty_obat: 2,
  };

  const onSubmit = (data) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    console.log(addResepData);
    var raw = `{
      "token":"SemogaDilancarkanSemuanayaAamiiin",
      "cmd":"${cmdTransaksi}",
      "mulai_tgl":"${mulai}",
      "sampai_tgl":"${sampaiTgl}",
      "clinic_code":"CN-BDG001",
      "ndata":""
    }`;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    console.log(raw);
    fetch("https://clinic.messapp.id/v1/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        console.log(result);
        let res = JSON.parse(result);
        console.log(res);
        setData([
          { name: mulai, trx: 0 },
          {
            name: sampaiTgl,
            trx: res.setvar.varvalue,
          },
        ]);
      })
      .catch((error) => {
        console.log("error", error);
        setIsErr(true);
        setMsg(error);
      });
  };

  console.log(data);

  return (
    <React.Fragment>
      <Title>Grafik Transaksi</Title>
      <ResponsiveContainer>
        <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={3}>
              <TextField
                id="transaksi"
                select
                fullWidth
                label="Pilih Transaksi"
                value={cmdTransaksi}
                onChange={(e) => {
                  setCmdTransaksi(e.target.value);
                }}
                helperText="Pilih Transaksi"
              >
                <MenuItem key="1" value="JumlahTransaksi">
                  Jumlah Transaksi
                </MenuItem>
                <MenuItem key="2" value="JumlahTransaksiKonsultasi">
                  Jumlah Transaksi Konsultasi
                </MenuItem>
                <MenuItem key="3" value="JumlahTransaksiPerawatan">
                  Jumlah Transaksi Perawatan
                </MenuItem>
                <MenuItem key="4" value="JumlahTransaksiTindakan">
                  Jumlah Transaksi Tindakan
                </MenuItem>
                <MenuItem key="5" value="TotalTransaksi">
                  Total Transaksi
                </MenuItem>
                <MenuItem key="6" value="TotalTransaksiKonsultasi">
                  Total Transaksi Konsultasi
                </MenuItem>
                <MenuItem key="7" value="TotalTransaksiPerawatan">
                  Total Transaksi Perawatan
                </MenuItem>
                <MenuItem key="8" value="TotalTransaksiTindakan">
                  Total Transaksi Tindakan
                </MenuItem>
              </TextField>
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                id="tglMulai"
                type="text"
                fullWidth
                type="date"
                label="Tanggal Mulai"
                InputLabelProps={{
                  shrink: true,
                }}
                value={mulai}
                onChange={(e) => setMulai(e.target.value)}
                autoFocus
                ref={register({
                  required: "Required",
                })}
              />
            </Grid>

            <Grid item xs={12} sm={3}>
              <TextField
                id="tglSelesai"
                type="text"
                fullWidth
                type="date"
                label="Sampai Tanggal"
                InputLabelProps={{
                  shrink: true,
                }}
                value={sampaiTgl}
                onChange={(e) => setSampaiTgl(e.target.value)}
                autoFocus
                ref={register({
                  required: "Required",
                })}
              />
            </Grid>

            <Grid item xs={12} sm={3}>
              <ButtonGroup
                color="primary"
                aria-label="outlined primary button group"
              >
                <Button variant="contained" color="primary" type="submit">
                  Cari
                </Button>
              </ButtonGroup>
            </Grid>
          </Grid>
          <LineChart
            width={600}
            height={300}
            data={data}
            margin={{ top: 5, right: 20, bottom: 5, left: 0 }}
          >
            <Line type="monotone" dataKey="trx" stroke="#82ca9d" />
            <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
          </LineChart>
        </form>
      </ResponsiveContainer>
    </React.Fragment>
  );
}
