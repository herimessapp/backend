import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";

import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { MenuItem } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function ClinicAdd() {
  const classes = useStyles();
  const history = useHistory();
  const [kodeDokter, setKodeDokter] = useState("");
  const [kodeKlinik, setKodeKlinik] = useState("");
  const [idKlinik, setIdKlinik] = useState("");
  const [getIdKlinik, setGetIdKlinik] = useState([]);
  const [getKodeKlinik, setGetKodeKlinik] = useState("");
  const [namaKlinik, setNamaKlinik] = useState("");
  const [namaDokter, setNamaDokter] = useState("");
  const [jenisKelamin, setJenisKelamin] = useState("");
  const [spesialis, setSpesialisDokter] = useState("");
  const [jadwalDokter, setJadwalDokter] = useState("");
  const [emailDokter, setEmailDokter] = useState("");
  const [waDokter, setWaDokter] = useState("");
  const [alamatDokter, setAlamatDokter] = useState("");
  const [statusDokter, setStatusDokter] = useState("");
  const [isSuccess, setIsSuccess] = useState(false);
  const [isErr, setIsErr] = useState(false);
  const [msg, setMsg] = useState("");
  const [isLoaded, setIsLoaded] = useState(false);
  const { register, handleSubmit } = useForm();

  let addClinicData = {
    clinicId: "",
    kodeDokter: "",
    kodeKlinik: "",
    namaDokter: "",
    jenisKelamin: "",
    spesialis: "",
    jadwalDokter: "",
    emailDokter: "",
    waDokter: "",
    alamatDokter: "",
    statusDokter: "",
  };

  const onSubmit = (data) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    console.log(addClinicData);
    const raw = `{
              "token":"SemogaDilancarkanSemuanayaAamiiin",
              "cmd":"IudDataDokter",
              "func":"insert",
              "clinic_id":"${addClinicData.clinicId}",
              "clinic_code":"${addClinicData.kodeKlinik}",
              "doctor_code":"${addClinicData.kodeDokter}",
              "name":"${addClinicData.namaDokter}",
              "gender":"${addClinicData.jenisKelamin}",
              "specialist":"${addClinicData.spesialis}",
              "schedule":"${addClinicData.jadwalDokter}",
              "email":"${addClinicData.emailDokter}",
              "wa_number":"${addClinicData.waDokter}",
              "address":"${addClinicData.alamatDokter}",
              "is_active":"${addClinicData.statusDokter}",
              "created_at":"2020-07-25 19:32:06"
            }`;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    console.log(raw);
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);
        console.log(res);

        if (res.error === "0") {
          setIsSuccess(true);
          setMsg(res.setvar.varvalue);
          setTimeout(() => {
            history.push("/dokter");
          }, 2000);
        } else {
          setIsSuccess(true);
          setMsg(res.setvar.varvalue);
        }
      })
      .catch((error) => {
        console.log("error", error);
        setIsErr(true);
        setMsg(error);
      });
  };

  const getIdClinic = () => {
    setIsLoaded(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw =
      '{"token":"SemogaDilancarkanSemuanayaAamiiin", "cmd":"LoadAllDataClinic", "cari":"", "posisi":0,"batas":100}';
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);

        if (res.error === 0) {
          let data = res.setvar.varvalue.map((item) => {
            return {
              id_clinic: item.id,
            };
          });

          setGetIdKlinik(data);
        } else {
          console.log(res);
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  const getCodeClinic = (id) => {
    setIsLoaded(true);

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw = `{"token":"SemogaDilancarkanSemuanayaAamiiin", "cmd":"LoadIdDataClinic", "id": ${id}}`;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);

        if (res.error === 0) {
          let data = res.setvar.varvalue;

          setGetKodeKlinik(data.code);
          setNamaKlinik(data.name);
        } else {
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  useEffect(() => {
    getIdClinic();
  }, []);

  useEffect(() => {
    addClinicData.clinicId = idKlinik;
  }, [addClinicData, idKlinik]);

  useEffect(() => {
    addClinicData.kodeDokter = kodeDokter;
  }, [addClinicData, kodeDokter]);

  useEffect(() => {
    addClinicData.kodeKlinik = getKodeKlinik;
  }, [addClinicData, getKodeKlinik]);

  useEffect(() => {
    addClinicData.namaDokter = namaDokter;
  }, [addClinicData, namaDokter]);

  useEffect(() => {
    addClinicData.jenisKelamin = jenisKelamin;
  }, [addClinicData, jenisKelamin]);

  useEffect(() => {
    addClinicData.spesialis = spesialis;
  }, [addClinicData, spesialis]);

  useEffect(() => {
    addClinicData.jadwalDokter = jadwalDokter;
  }, [addClinicData, jadwalDokter]);

  useEffect(() => {
    addClinicData.emailDokter = emailDokter;
  }, [addClinicData, emailDokter]);

  useEffect(() => {
    addClinicData.waDokter = waDokter;
  }, [addClinicData, waDokter]);

  useEffect(() => {
    addClinicData.alamatDokter = alamatDokter;
  }, [addClinicData, alamatDokter]);

  useEffect(() => {
    addClinicData.statusDokter = statusDokter;
  }, [addClinicData, statusDokter]);

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const handleCloseSnack = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setIsSuccess(false);
    setIsErr(false);
  };

  const jk = [
    {
      value: 1,
      label: "Laki-laki",
    },
    {
      value: 0,
      label: "Perempuan",
    },
  ];

  const status = [
    {
      value: 1,
      label: "Aktif",
    },
    {
      value: 0,
      label: "Tidak Aktif",
    },
  ];

  const succcesMsg = (
    <Snackbar
      open={isSuccess}
      autoHideDuration={6000}
      onClose={handleCloseSnack}
    >
      <Alert onClose={handleCloseSnack} severity="success">
        {msg}
      </Alert>
    </Snackbar>
  );

  const errMsg = (
    <Snackbar open={isErr} autoHideDuration={6000} onClose={handleCloseSnack}>
      <Alert onClose={handleCloseSnack} severity="error">
        {msg}
      </Alert>
    </Snackbar>
  );

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Tambah Data Dokter
      </Typography>
      <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <TextField
              id="idKlinik"
              select
              fullWidth
              label="ID Klinik"
              value={idKlinik}
              onChange={(e) => {
                setIdKlinik(e.target.value);
                getCodeClinic(e.target.value);
              }}
              helperText="Pilih ID Klinik"
            >
              {getIdKlinik.map((option) => (
                <MenuItem key={option.id_clinic} value={option.id_clinic}>
                  {option.id_clinic}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="kodeDokter"
              label="Kode Dokter"
              name="nama_dokter"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setKodeDokter(e.target.value)}
              value={kodeDokter}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              id="kodeKlinik"
              type="text"
              fullWidth
              label={getKodeKlinik}
              value={
                getKodeKlinik !== ""
                  ? `${getKodeKlinik} ( ${namaKlinik} )`
                  : "Pilih ID klinik Terlebih Dahulu"
              }
              onChange={(e) => setGetKodeKlinik(e.target.value)}
              helperText="Kode Klinik, otomatis terisi ketika memilih ID"
              disabled
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="namaDokter"
              label="Nama Dokter"
              name="nama_dokter"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setNamaDokter(e.target.value)}
              value={namaDokter}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              id="jenisKelamin"
              select
              fullWidth
              label="Jenis Kelamin"
              value={jenisKelamin}
              onChange={(e) => setJenisKelamin(e.target.value)}
              helperText="Pilih Jenis Kelamin"
            >
              {jk.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="spesialis"
              label="Spesialis Dokter"
              name="spesialis_dokter"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setSpesialisDokter(e.target.value)}
              value={spesialis}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="jadwalPraktek"
              label="Jadwal Praktek"
              name="jadwalPraktek"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setJadwalDokter(e.target.value)}
              value={jadwalDokter}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="email"
              label="Email Dokter"
              name="email_dokter"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setEmailDokter(e.target.value)}
              value={emailDokter}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="noWa"
              label="Nomer Wa"
              name="nomerWa"
              fullWidth
              margin="normal"
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setWaDokter(e.target.value)}
              value={waDokter}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="alamat"
              label="Alamat Dokter"
              name="alamatDokter"
              fullWidth
              margin="normal"
              type="text"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setAlamatDokter(e.target.value)}
              value={alamatDokter}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              id="statusDokter"
              select
              label="Status Dokter"
              fullWidth
              value={statusDokter}
              onChange={(e) => setStatusDokter(e.target.value)}
              helperText="Pilih Status Dokter"
            >
              {status.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
          </Grid>

          <Grid item xs={12}>
            <ButtonGroup
              color="primary"
              aria-label="outlined primary button group"
            >
              <Button
                onClick={() => {
                  history.push("/backend/dokter");
                }}
              >
                Cancel
              </Button>
              <Button variant="contained" color="primary" type="submit">
                Save
              </Button>
            </ButtonGroup>
          </Grid>
        </Grid>
      </form>
      {succcesMsg}
      {errMsg}
    </React.Fragment>
  );
}
