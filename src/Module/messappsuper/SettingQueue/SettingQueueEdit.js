import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";

import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { MenuItem } from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";

import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
} from "@material-ui/pickers";

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function ConsultationAdd() {
  const classes = useStyles();
  const history = useHistory();
  const [kodeKlinik, setKodeKlinik] = useState("");
  const [getCodeKlinik, setGetCodeKlinik] = useState([]);
  const [personaKb, setPersonaKb] = useState("");
  const [grupAntrian, setGrupAntrian] = useState("");
  const [noAntrian, setNoAntrian] = useState("");
  const [typeAntrian, setTypeAntrian] = useState("");
  const [jamMulai, setJamMulai] = useState("");
  const [jamSelesai, setJamSelesai] = useState("");
  const [isSuccess, setIsSuccess] = useState(false);
  const [isErr, setIsErr] = useState(false);
  const [msg, setMsg] = useState("");
  const [isLoaded, setIsLoaded] = useState(false);
  const { idSettingAntrian } = useParams();

  const { register, handleSubmit } = useForm();

  let addSettingQueue = {
    clinic_code: "",
    persona_kb: "",
    queue_group: "",
    no_queue: "",
    type_queue: "",
    start_time: "",
    end_time: "",
  };

  const onSubmit = (data) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    console.log(addSettingQueue);
    const raw = `{
              "token":"SemogaDilancarkanSemuanayaAamiiin",
              "cmd":"IudDataSettingJadwal",
              "func":"update",
              "id": "${idSettingAntrian}",
              "clinic_code":"${addSettingQueue.clinic_code}",
              "persona_kb":"${addSettingQueue.persona_kb}",
              "queue_group":"${addSettingQueue.queue_group}",
              "no_queue":"${addSettingQueue.no_queue}",
              "type_queue":"${addSettingQueue.type_queue}",
              "start_time":"${addSettingQueue.start_time}",
              "end_time":"${addSettingQueue.end_time}"
            }`;
    console.log(raw);
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        console.log(result);
        let res = JSON.parse(result);
        console.log(res);

        if (res.error === "0") {
          setIsSuccess(true);
          setMsg(res.setvar.varvalue);
          setTimeout(() => {
            history.push("/setting-antrian");
          }, 2000);
        } else {
          setIsSuccess(true);
          setMsg(res.setvar.varvalue);
        }
      })
      .catch((error) => {
        console.log("error", error);
        setIsErr(true);
        setMsg(error);
      });
  };

  const getCodeClinicAPI = () => {
    setIsLoaded(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw =
      '{"token":"SemogaDilancarkanSemuanayaAamiiin", "cmd":"LoadAllDataClinic", "cari":"", "posisi":0,"batas":100}';
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);

        if (res.error === 0) {
          let data = res.setvar.varvalue.map((item) => {
            return {
              code_clinic: item.code,
            };
          });

          setGetCodeKlinik(data);
        } else {
          console.log(res);
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  const getIdDataSettingQueue = () => {
    setIsLoaded(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw = `{"token":"SemogaDilancarkanSemuanayaAamiiin", "cmd":"LoadIdDataSettingJadwal", "id":"${idSettingAntrian}"}`;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);

        if (res.error === 0) {
          let data = res.setvar.varvalue;
          setKodeKlinik(data.clinic_code);
          setNoAntrian(data.no_queue);
          setTypeAntrian(data.type_queue);
          setGrupAntrian(data.queue_group);
          setJamMulai(data.start_time);
          setJamSelesai(data.end_time);
          setPersonaKb(data.persona_kb);
        } else {
          console.log(res);
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  useEffect(() => {
    getCodeClinicAPI();
    getIdDataSettingQueue();
  }, []);

  useEffect(() => {
    addSettingQueue.clinic_code = kodeKlinik;
  }, [addSettingQueue, kodeKlinik]);

  useEffect(() => {
    addSettingQueue.persona_kb = personaKb;
  }, [addSettingQueue, personaKb]);

  useEffect(() => {
    addSettingQueue.queue_group = grupAntrian;
  }, [addSettingQueue, grupAntrian]);

  useEffect(() => {
    addSettingQueue.no_queue = noAntrian;
  }, [addSettingQueue, noAntrian]);

  useEffect(() => {
    addSettingQueue.type_queue = typeAntrian;
  }, [addSettingQueue, typeAntrian]);

  useEffect(() => {
    addSettingQueue.start_time = jamMulai;
  }, [addSettingQueue, jamMulai]);

  useEffect(() => {
    addSettingQueue.end_time = jamSelesai;
  }, [addSettingQueue, jamSelesai]);

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const handleCloseSnack = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setIsSuccess(false);
    setIsErr(false);
  };

  const succcesMsg = (
    <Snackbar
      open={isSuccess}
      autoHideDuration={6000}
      onClose={handleCloseSnack}
    >
      <Alert onClose={handleCloseSnack} severity="success">
        {msg}
      </Alert>
    </Snackbar>
  );

  const errMsg = (
    <Snackbar open={isErr} autoHideDuration={6000} onClose={handleCloseSnack}>
      <Alert onClose={handleCloseSnack} severity="error">
        {msg}
      </Alert>
    </Snackbar>
  );

  const typeGrup = [
    {
      value: "Pagi",
      label: "Pagi",
    },
    {
      value: "Sore",
      label: "Sore",
    },
  ];

  const typeAntrianList = [
    {
      value: "K",
      label: "Konsultasi",
    },
    {
      value: "B",
      label: "Bertemu Dokter",
    },
    {
      value: "P",
      label: "Perawatan",
    },
  ];

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Edit Data Setting Antrian
      </Typography>
      <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={6}>
              <TextField
                id="kodeKlinik"
                select
                fullWidth
                label="Kode Klinik"
                value={kodeKlinik}
                onChange={(e) => {
                  setKodeKlinik(e.target.value);
                }}
                helperText="Pilih Kode Klinik"
              >
                {getCodeKlinik.map((option) => (
                  <MenuItem key={option.code_clinic} value={option.code_clinic}>
                    {option.code_clinic}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="personaKb"
                label="Persona KB"
                name="persona_kb"
                fullWidth
                margin="normal"
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={(e) => setPersonaKb(e.target.value)}
                value={personaKb}
                autoFocus
                ref={register({
                  required: "Required",
                })}
              />
            </Grid>

            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="noAntrian"
                label="Nomer Antrian"
                name="persona_kb"
                fullWidth
                type="number"
                margin="normal"
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={(e) => setNoAntrian(e.target.value)}
                value={noAntrian}
                autoFocus
                ref={register({
                  required: "Required",
                })}
              />
            </Grid>

            <Grid item xs={12} sm={6}>
              <TextField
                id="typeGroup"
                select
                fullWidth
                label="Grup Antrian"
                value={grupAntrian}
                onChange={(e) => {
                  setGrupAntrian(e.target.value);
                }}
                helperText="Pilih Grup Antrian"
              >
                {typeGrup.map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>

            <Grid item xs={12} sm={6}>
              <TextField
                id="typeAntrian"
                select
                fullWidth
                label="Type Antrian"
                value={typeAntrian}
                onChange={(e) => {
                  setTypeAntrian(e.target.value);
                }}
                helperText="Pilih Type Antrian"
              >
                {typeAntrianList.map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>

            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="jamMulai"
                label="Jam Mulai"
                name="jam_mulai"
                fullWidth
                type="time"
                margin="normal"
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={(e) => setJamMulai(e.target.value)}
                value={jamMulai}
                autoFocus
                ref={register({
                  required: "Required",
                })}
              />
            </Grid>

            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="jamSelesai"
                label="Jam Selesai"
                name="jam_selesai"
                fullWidth
                type="time"
                margin="normal"
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={(e) => setJamSelesai(e.target.value)}
                value={jamSelesai}
                autoFocus
                ref={register({
                  required: "Required",
                })}
              />
            </Grid>

            <Grid item xs={12}>
              <ButtonGroup
                color="primary"
                aria-label="outlined primary button group"
              >
                <Button
                  onClick={() => {
                    history.push("/backend/setting-antrian");
                  }}
                >
                  Cancel
                </Button>
                <Button variant="contained" color="primary" type="submit">
                  Save
                </Button>
              </ButtonGroup>
            </Grid>
          </Grid>
        </MuiPickersUtilsProvider>
      </form>
      {succcesMsg}
      {errMsg}
    </React.Fragment>
  );
}
