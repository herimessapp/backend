import React, { useState, useEffect, forwardRef } from "react";
import { useHistory } from "react-router-dom";
import Snackbar from "@material-ui/core/Snackbar";

import { makeStyles } from "@material-ui/core/styles";
import MuiAlert from "@material-ui/lab/Alert";
import Grid from "@material-ui/core/Grid";
import Title from "../Title";
import Button from "@material-ui/core/Button";

import * as FileSaver from "file-saver";
import * as XLSX from "xlsx";

import MaterialTable from "material-table";
import {
  AddBox,
  ArrowDownward,
  Check,
  Clear,
  DeleteOutline,
  ChevronRight,
  Edit,
  SaveAlt,
  FilterList,
  FirstPage,
  LastPage,
  ChevronLeft,
  Search,
  Remove,
  ViewColumn,
} from "@material-ui/icons";

import IconButton from "@material-ui/core/IconButton";

function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function Doctor() {
  const history = useHistory();
  const [rows, setRows] = useState([]);
  const [error] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const [isErr, setIsErr] = useState(false);
  const [msg, setMsg] = useState("");

  const getRows = () => {
    setRows([]);
    setIsLoaded(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw = `
        {
        "token": "SemogaDilancarkanSemuanayaAamiiin",
        "cmd": "getTrxKonsultasi",
        "clinic_code": "CN-BDG001",
        "tanggal_mulai": "2020-08-01",
        "tanggal_akhir": "2020-08-20",
        "status_transaksi": "",
        "shop_code": "",
        "cari": "",
        "posisi": 0,
        "batas": 100
    }
      `;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);
        if (res.error === 0) {
          let data = res.setvar.varvalue.map((item) => {
            console.log(item);
            return {
              id: item.id,
              invoice_number: item.invoice_number,
              clinic_code: item.clinic_code,
              clinic_name: item.clinic_name,
              consultation_type: item.consultation_type,
              schedule_date: item.schedule_date,
              antrian: item.antrian,
              doctor_name: item.doctor_name,
              wa_patient: item.wa_patient,
              patient_name: item.patient_name,
              transaction_date: item.transaction_date,
              stat: item.stat,
              total: item.total,
              payment_methode: item.payment_methode,
              linkpg: item.linkpg,
              response_gp: item.response_gp,
            };
          });
          console.log(data);
          setRows(data);
        } else {
          console.log(res);
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  useEffect(() => {
    getRows();
  }, []);

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const handleCloseSnack = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setIsSuccess(false);
    setIsErr(false);
  };

  const fileType =
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
  const fileExtension = ".xlsx";

  const exportToCSV = (csvData, fileName) => {
    const ws = XLSX.utils.json_to_sheet(csvData);
    const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
    const excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" });
    const data = new Blob([excelBuffer], { type: fileType });
    FileSaver.saveAs(data, fileName + fileExtension);
  };

  const succcesMsg = (
    <Snackbar
      open={isSuccess}
      autoHideDuration={6000}
      onClose={handleCloseSnack}
    >
      <Alert onClose={handleCloseSnack} severity="success">
        {msg}
      </Alert>
    </Snackbar>
  );

  const errMsg = (
    <Snackbar open={isErr} autoHideDuration={6000} onClose={handleCloseSnack}>
      <Alert onClose={handleCloseSnack} severity="error">
        {msg}
      </Alert>
    </Snackbar>
  );

  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {
    return (
      <React.Fragment>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <Title>Data Transaksi Konsultasi</Title>
            <Button
              variant="contained"
              color="primary"
              size="small"
              style={{ marginBottom: "10px" }}
              endIcon={<i class="ri-download-2-line"></i>}
              onClick={() => exportToCSV(rows, "dataTransaksiResep")}
            >
              Export
            </Button>
          </Grid>
        </Grid>
        <MaterialTable
          icons={tableIcons}
          options={{
            paging: true,
            sorting: true,
            headerStyle: {
              backgroundColor: "#01579b",
              color: "#FFF",
            },
          }}
          title=""
          columns={[
            { title: "Invoice Number", field: "invoice_number" },
            { title: "Kode Klinik ", field: "clinic_code" },
            { title: "Kode Nama", field: "clinic_name" },
            { title: "Type Konsultasi", field: "consultation_type" },
            { title: "Tanggal Jadwal", field: "schedule_date" },
            { title: "Antrian", field: "antrian" },
            { title: "Nama Dokter", field: "doctor_name" },
            { title: "Wa Pasien", field: "wa_patient" },
            { title: "Nama Pasien", field: "patient_name" },
            { title: "Tanggal Transaksi", field: "transaction_date" },
            { title: "Status", field: "stat" },
            { title: "Total", field: "total" },
            { title: "Payment Metode", field: "payment_methode" },
            { title: "Link Payment", field: "linkpg" },
            { title: "Response Gp", field: "response_gp" },
          ]}
          data={rows}
        />
        {succcesMsg}
        {errMsg}
      </React.Fragment>
    );
  }
}
