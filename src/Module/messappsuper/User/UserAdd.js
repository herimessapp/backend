import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import ApplicationBar from "../../../component/ApplicationBar";
import DrawerSuper from "../DrawerSuper";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Copyright from "../../../component/Copyright";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";

import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: "none",
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function UserAdd(props) {
  const classes = useStyles();
  const history = useHistory();

  const [open, setOpen] = useState(true);
  const [email, setEmail] = useState("");
  const [wa, setWa] = useState("");
  const [clinic_id, setclinic_id] = useState(null);
  const [dataklinik, setdataklinik] = useState("");
  const [role, setRole] = useState(0);
  const [isSuccess, setIsSuccess] = useState(false);
  const [isErr, setIsErr] = useState(false);
  const [msg, setMsg] = useState("");
  const { register, handleSubmit } = useForm();

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setIsSuccess(false);
    setIsErr(false);
  };

  const handleRole = (event) => {
    setRole(event.target.value);
  };

  const succcesMsg = (
    <Snackbar open={isSuccess} autoHideDuration={6000} onClose={handleClose}>
      <Alert onClose={handleClose} severity="success">
        {msg}
      </Alert>
    </Snackbar>
  );

  const errMsg = (
    <Snackbar open={isErr} autoHideDuration={6000} onClose={handleClose}>
      <Alert onClose={handleClose} severity="error">
        {msg}
      </Alert>
    </Snackbar>
  );

  const onSubmit = (data) => {
    if (role > 1 && clinic_id === null) {
      setIsErr(true);
      setMsg("Klinik harus dipilih");
    } else {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "text/plain");
      var raw =
        '{"token":"SemogaDilancarkanSemuanayaAamiiin","cmd":"IudDataUser","func":"insert",' +
        '"email":"' +
        email +
        '",' +
        '"wa":"' +
        wa +
        '",' +
        '"clinic_id":' +
        clinic_id +
        "," +
        '"ndata":"",' +
        '"role":' +
        role +
        "}";
      var requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };
      fetch("https://clinic.messapp.id/v1/index.php", requestOptions)
        .then((response) => response.text())
        .then((result) => {
          let res = JSON.parse(result);
          if (res.error === "0") {
            setIsSuccess(true);
            setMsg(res.setvar.varvalue);
          } else {
            setIsErr(true);
            setMsg(res.setvar.varvalue);
          }
        })
        .catch((error) => {
          setIsErr(true);
          setMsg(JSON.stringify(error));
        });
    }
  };

  useEffect(() => {
    if (dataklinik === "") {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "text/plain");
      var raw =
        '{"token":"SemogaDilancarkanSemuanayaAamiiin", "cmd":"LoadAllDataClinic", "cari":"", "posisi":0,"batas":10000000}';
      var requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };
      fetch("https://clinic.messapp.id/v1/index.php", requestOptions)
        .then((response) => response.text())
        .then((result) => {
          let res = JSON.parse(result);
          if (res.error === 0) {
            res.setvar.varvalue.unshift({ id: null, code: "", name: "" });
            setdataklinik(res.setvar.varvalue);
          } else {
            setMsg(res.setvar.varvalue);
            setIsErr(true);
          }
        })
        .catch((error) => console.log(JSON.stringify(error)));
    }
  }, []);

  return (
    <div className={classes.root}>
      <CssBaseline />
      <ApplicationBar open={open} setOpen={setOpen} />
      <DrawerSuper open={open} setOpen={setOpen} />
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Paper className={classes.paper}>
                <Typography variant="h6" gutterBottom>
                  Tambah User
                </Typography>
                <form
                  className={classes.form}
                  onSubmit={handleSubmit(onSubmit)}
                >
                  <Grid container spacing={3}>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        name="email"
                        id="email"
                        label="Email"
                        fullWidth
                        margin="normal"
                        InputLabelProps={{
                          shrink: true,
                        }}
                        onChange={(e) => setEmail(e.target.value)}
                        value={email}
                        ref={register({
                          required: "Required",
                          pattern: {
                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                            message: "invalid email address",
                          },
                        })}
                        type="email"
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="wa"
                        label="Nomor WA"
                        name="wa"
                        fullWidth
                        margin="normal"
                        InputLabelProps={{
                          shrink: true,
                        }}
                        onChange={(e) => setWa(e.target.value)}
                        value={wa}
                        ref={register({
                          required: "Required",
                        })}
                        type="tel"
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <FormControl required className={classes.formControl}>
                        <InputLabel htmlFor="clinic_id">Klinik</InputLabel>
                        <Select
                          native
                          value={clinic_id}
                          onChange={(event) => {
                            if (event.target.value === "-") {
                              setclinic_id(null);
                            } else {
                              setclinic_id(event.target.value);
                            }
                          }}
                          name="clinic_id"
                          inputProps={{
                            id: "clinic_id",
                          }}
                          fullWidth
                          displayEmpty
                          id="clinic_id"
                        >
                          {dataklinik !== "" &&
                            dataklinik.map((val) => {
                              return (
                                <option value={val.id}>
                                  {val.code} - {val.name}
                                </option>
                              );
                            })}
                        </Select>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <FormControl required className={classes.formControl}>
                        <InputLabel htmlFor="role">Role</InputLabel>
                        <Select
                          native
                          value={role}
                          onChange={handleRole}
                          name="role"
                          inputProps={{
                            id: "role",
                          }}
                          fullWidth
                          displayEmpty
                        >
                          <option value="0">Administrator</option>
                          <option value="1">Customer Service</option>
                          <option value="2">Dokter </option>
                          <option value="3">Apotek </option>
                          <option value="4">Rekam Medik </option>
                          <option value="5">Antrian </option>
                          <option value="6">Tindakan </option>
                        </Select>
                      </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                      <ButtonGroup
                        color="primary"
                        aria-label="outlined primary button group"
                      >
                        <Button
                          onClick={() => {
                            history.push("/backend/user");
                          }}
                        >
                          Close
                        </Button>
                        <Button
                          variant="contained"
                          color="primary"
                          type="submit"
                        >
                          Save
                        </Button>
                      </ButtonGroup>
                    </Grid>
                  </Grid>
                </form>
                {succcesMsg}
                {errMsg}
              </Paper>
            </Grid>
          </Grid>
          <Box pt={4}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
}
