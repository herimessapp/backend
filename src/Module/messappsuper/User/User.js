import React, { useState, useEffect, forwardRef} from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { useHistory } from "react-router-dom"
import CssBaseline from '@material-ui/core/CssBaseline';
import ApplicationBar from '../../../component/ApplicationBar'
import DrawerSuper from '../DrawerSuper'
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Copyright from '../../../component/Copyright'
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import MaterialTable from 'material-table';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

export default function User(props) {
  const classes = useStyles();
  const history = useHistory();
  const theme = useTheme()
  const [rows, setRows] = useState([])
  const [error] = useState(null);
  const [isSuccess, setIsSuccess] = useState(false)
  const [isErr, setIsErr] = useState(false)
  const [msg, setMsg] = useState('')

  const getRows = () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw = '{"token":"SemogaDilancarkanSemuanayaAamiiin", "cmd":"LoadAllDataUser", "cari":"", "posisi":0, "batas":10000000}'    
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    fetch("https://clinic.messapp.id/v1/index.php", requestOptions)
      .then(response => response.text())
      .then(result => {
        let res = JSON.parse(result)
        if (res.error === 0) {
          setRows(res.setvar.varvalue);
        } else {
          setMsg(res.setvar.varvalue)
          setIsErr(true)
        }
      })
      .catch(error => console.log(JSON.stringify(error)));
  }

  useEffect(() => {
    getRows()
  }, [])

  const handleCloseSnack = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setIsSuccess(false);
    setIsErr(false)
  };

  const succcesMsg = <Snackbar open={isSuccess} autoHideDuration={6000} onClose={handleCloseSnack}>
    <Alert onClose={handleCloseSnack} severity="success">
      {msg}
    </Alert>
  </Snackbar>

  const errMsg = <Snackbar open={isErr} autoHideDuration={6000} onClose={handleCloseSnack}>
    <Alert onClose={handleCloseSnack} severity="error">
      {msg}
    </Alert>
  </Snackbar>

  if (error) {
    return <div>Error: {error.message}</div>;
  } else {
    return (
      <div className={classes.root}>
        <CssBaseline />
          <ApplicationBar open={props.open} setOpen={props.setOpen} />
          <DrawerSuper  open={props.open} setOpen={props.setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  {rows !== '' && <MaterialTable
                    icons={tableIcons}
                    options={{
                      paging: true,
                      sorting: true,
                      headerStyle: {
                        backgroundColor: '#01579b',
                        color: '#FFF'
                      }
                    }}
                    title="User"
                    columns={[
                      { title: 'Email', field: 'email' },
                      { title: 'WA', field: 'wa' },
                      { title: 'Role', field: 'role',
                        render: (rowData) => {
                          switch (rowData.role) {
                            case 0 : return <Box>Messapp RIUD</Box>
                            case 1 : return <Box>Messapp Read only</Box>
                            case 2 : return <Box>Dokter</Box>
                            case 3 : return <Box>Apotek</Box>
                            case 4 : return <Box>Rekam Medis</Box>
                            case 5 : return <Box>Antrian</Box>
                            case 6 : return <Box>Tindakan</Box>
                            default : return <Box>?</Box>
                          }
                        }
                      },
                      { title: 'Klinik', field: 'name' },
                      {
                        field: 'id',
                        title: 'Action',
                        cellStyle: {
                          textAlign: 'right',
                        },
                        headerStyle: {
                          textAlign: 'right',
                        },
                        render: rowData => (
                          <>
                            <IconButton color="secondary" aria-label="add an alarm" onClick={() => {
                                // TO DO with dialog
                                var myHeaders = new Headers();
                                myHeaders.append("Content-Type", "text/plain");
                                var raw = '{"token":"SemogaDilancarkanSemuanayaAamiiin","cmd":"DelUser",' +
                                  '"id":'+ rowData.id +',' +
                                  '"ndata":""}'    
                                var requestOptions = {
                                  method: 'POST',
                                  headers: myHeaders,
                                  body: raw,
                                  redirect: 'follow'
                                };
                                fetch("https://clinic.messapp.id/v1/index.php", requestOptions)
                                  .then(response => response.text())
                                  .then(result => {
                                    let res = JSON.parse(result)
                                    if (res.error === "0") {
                                      getRows()
                                      setMsg(res.setvar.varvalue)
                                      setIsSuccess(true)
                                    } else {
                                      setMsg(res.setvar.varvalue)
                                      setIsErr(true)
                                    }
                                  })
                                  .catch(error => console.log('error', error));
                            }}>
                              <DeleteIcon />
                            </IconButton>
                            <IconButton color="default" aria-label="add an alarm" onClick={() => {history.push("/backend/user/edit/" + rowData.id);}} >
                              <EditIcon />
                            </IconButton>
                          </>
                        )
                      }
                    ]}
                    data={rows}
                  />
                  }
                  <Fab color="primary" aria-label="add" className={classes.margin} style={{
                      position: 'absolute',
                      bottom: theme.spacing(2),
                      right: theme.spacing(2),
                    }}>
                    <AddIcon onClick={() => {
                      history.push("/backend/user/add");}} />
                  </Fab>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
              {succcesMsg}
              {errMsg}
            </Container>
          </main>
        </div>
    );
  }
}
