import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";

import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function ClinicEdit() {
  const classes = useStyles();
  const history = useHistory();
  const [rows, setRows] = useState([]);
  const [kodeKlinik, setKodeKlinik] = useState("");
  const [personaKb, setPersonaKb] = useState("");
  const [registerDate, setRegisterDate] = useState("");
  const [namaKlinik, setNamaKlinik] = useState("");
  const [typeKlinik, setTypeKlinik] = useState("");
  const [direkturKlinik, setDirekturKlinik] = useState("");
  const [organizerKlinik, setOrganizerKlinik] = useState("");
  const [alamatKlinik, setAlamatKlinik] = useState("");
  const [kotaKlinik, setKotaKlinik] = useState("");
  const [teleponKlinik, setTeleponKlinik] = useState("");
  const [waKlinik, setWaKlinik] = useState("");
  const [isSuccess, setIsSuccess] = useState(false);
  const [isErr, setIsErr] = useState(false);
  const [msg, setMsg] = useState("");
  const { idClinic } = useParams();

  const [isLoaded, setIsLoaded] = useState(false);
  const { register, handleSubmit } = useForm();

  let addClinicData = {
    code: "12",
    persona_kb: "x",
    register_date: "q",
    name: "q",
    type: "q",
    director: "q",
    organizer: "q",
    address: "q",
    city: "q",
    phone: "q",
    wa_number: "q",
    ndata: "",
  };

  const onSubmit = (data) => {
    console.log(data);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: `{"token":"SemogaDilancarkanSemuanayaAamiiin","cmd":"IudDataClinic","func":"update", "id":"${idClinic}", "code":"${addClinicData.code}","persona_kb":"${addClinicData.persona_kb}","register_date":"${addClinicData.register_date}","name":"${addClinicData.name}","type":"${addClinicData.type}","director":"${addClinicData.director}","organizer":"${addClinicData.organizer}","address":"${addClinicData.address}","city":"${addClinicData.city}","phone":"${addClinicData.phone}","wa_number":"${addClinicData.wa_number}","ndata":""}`,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);
        if (res.error === "0") {
          setIsSuccess(true);
          setMsg(res.setvar.varvalue);
          setTimeout(() => {
            history.push("/klinik");
          }, 2000);
        } else {
          setIsSuccess(true);
          setMsg(res.setvar.varvalue);
        }
      })
      .catch((error) => {
        console.log("error", error);
        setIsErr(true);
        setMsg(error);
      });
  };

  const getRows = () => {
    setRows([]);
    setIsLoaded(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw = `{"token":"SemogaDilancarkanSemuanayaAamiiin", "cmd":"LoadIdDataClinic", "id":"${idClinic}"}`;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v1/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);
        if (res.error === 0) {
          let data = res.setvar.varvalue;
          setKodeKlinik(data.code);
          setPersonaKb(data.persona_kb);
          setRegisterDate(data.register_date);
          setNamaKlinik(data.name);
          setTypeKlinik(data.type);
          setDirekturKlinik(data.director);
          setOrganizerKlinik(data.organizer);
          setAlamatKlinik(data.address);
          setKotaKlinik(data.city);
          setTeleponKlinik(data.phone);
          setWaKlinik(data.wa_number);
        } else {
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  useEffect(() => {
    addClinicData.code = kodeKlinik;
  }, [addClinicData, kodeKlinik]);

  useEffect(() => {
    addClinicData.persona_kb = personaKb;
  }, [addClinicData, personaKb]);

  useEffect(() => {
    addClinicData.register_date = registerDate;
  }, [addClinicData, registerDate]);

  useEffect(() => {
    addClinicData.name = namaKlinik;
  }, [addClinicData, namaKlinik]);

  useEffect(() => {
    addClinicData.type = typeKlinik;
  }, [addClinicData, typeKlinik]);

  useEffect(() => {
    addClinicData.director = direkturKlinik;
  }, [addClinicData, direkturKlinik]);

  useEffect(() => {
    addClinicData.organizer = organizerKlinik;
  }, [addClinicData, organizerKlinik]);

  useEffect(() => {
    addClinicData.address = alamatKlinik;
  }, [addClinicData, alamatKlinik]);

  useEffect(() => {
    addClinicData.city = kotaKlinik;
  }, [addClinicData, kotaKlinik]);

  useEffect(() => {
    addClinicData.phone = teleponKlinik;
  }, [addClinicData, teleponKlinik]);

  useEffect(() => {
    addClinicData.wa_number = waKlinik;
  }, [addClinicData, waKlinik]);

  useEffect(() => {
    getRows();
  }, []);

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const handleCloseSnack = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setIsSuccess(false);
    setIsErr(false);
  };

  const succcesMsg = (
    <Snackbar
      open={isSuccess}
      autoHideDuration={6000}
      onClose={handleCloseSnack}
    >
      <Alert onClose={handleCloseSnack} severity="success">
        {msg}
      </Alert>
    </Snackbar>
  );

  const errMsg = (
    <Snackbar open={isErr} autoHideDuration={6000} onClose={handleCloseSnack}>
      <Alert onClose={handleCloseSnack} severity="error">
        {msg}
      </Alert>
    </Snackbar>
  );

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Edit Data Klinik
      </Typography>
      <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="kodeKlinik"
              label="Kode Klinik"
              name="kode_klinik"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setKodeKlinik(e.target.value)}
              value={kodeKlinik}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="personaKb"
              label="Persona Kb"
              name="persona_kb"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setPersonaKb(e.target.value)}
              value={personaKb}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="registerDate"
              label="Tanggal Registrasi"
              name="register_date"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setRegisterDate(e.target.value)}
              value={registerDate}
              autoFocus
              type="date"
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="namaKlinik"
              label="Nama Klinik"
              name="nama_klinik"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setNamaKlinik(e.target.value)}
              value={namaKlinik}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="typeKlinik"
              label="Type Klinik"
              name="type_klinik"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setTypeKlinik(e.target.value)}
              value={typeKlinik}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="direkturKlinik"
              label="Direktur Klinik"
              name="direktur_klinik"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setDirekturKlinik(e.target.value)}
              value={direkturKlinik}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="organizerKlinik"
              label="Organizer Klinik"
              name="organizer_klinik"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setOrganizerKlinik(e.target.value)}
              value={organizerKlinik}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="alamatKlinik"
              label="Alamat Klinik"
              name="alamat_klinik"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setAlamatKlinik(e.target.value)}
              value={alamatKlinik}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="kotaKlinik"
              label="Kota Klinik"
              name="kota_klinik"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setKotaKlinik(e.target.value)}
              value={kotaKlinik}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="teleponKlinik"
              label="Telepon Klinik"
              name="telepon_klinik"
              fullWidth
              margin="normal"
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setTeleponKlinik(e.target.value)}
              value={teleponKlinik}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="waKlinik"
              label="Nomer Wa Klinik"
              name="wa_klinik"
              fullWidth
              margin="normal"
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setWaKlinik(e.target.value)}
              value={waKlinik}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12}>
            <ButtonGroup
              color="primary"
              aria-label="outlined primary button group"
            >
              <Button
                onClick={() => {
                  history.push("/backend/klinik");
                }}
              >
                Cancel
              </Button>
              <Button variant="contained" color="primary" type="submit">
                Save
              </Button>
            </ButtonGroup>
          </Grid>
        </Grid>
      </form>
      {succcesMsg}
      {errMsg}
    </React.Fragment>
  );
}
