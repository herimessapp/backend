import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";

import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { MenuItem } from "@material-ui/core";
import { useParams } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function ClinicAdd() {
  const classes = useStyles();
  const history = useHistory();
  const [kodeKlinik, setKodeKlinik] = useState("");
  const [getCodeKlinik, setGetCodeKlinik] = useState([]);
  const [idRekamMedis, setIdRekamMedis] = useState("");
  const [getIdRekamMedis, setGetIdRekamMedis] = useState([]);
  const [idObat, setIdObat] = useState("");
  const [getIdObat, setGetIdObat] = useState([]);
  const [jumlahObat, setJumlahObat] = useState("");

  const [isSuccess, setIsSuccess] = useState(false);
  const [isErr, setIsErr] = useState(false);
  const [msg, setMsg] = useState("");
  const [isLoaded, setIsLoaded] = useState(false);
  const { register, handleSubmit } = useForm();
  const { idResep } = useParams();

  let addResepData = {
    clinic_code: "CN-BDG001",
    id_rekam_medis: "2",
    id_obat: 1,
    qty_obat: 2,
  };

  const onSubmit = (data) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    console.log(addResepData);
    var raw = `{
    "token":"SemogaDilancarkanSemuanayaAamiiin",
    "cmd":"IudDataResep",
    "func":"update",
    "clinic_code":"CN-BDG001",
    "id_rekam_medis":"${idResep}",
    "id_obat":[
        {
            "id":${addResepData.id_obat},
            "qyt":${addResepData.qty_obat}            
        }
    ]
}`;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    console.log(raw);
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        console.log(result);
        let res = JSON.parse(result);
        console.log(res);

        if (res.error === "0") {
          setIsSuccess(true);
          setMsg(res.setvar.varvalue);
          setTimeout(() => {
            history.push("/backend/resep");
          }, 2000);
        } else {
          setIsSuccess(true);
          setMsg(res.setvar.varvalue);
        }
      })
      .catch((error) => {
        console.log("error", error);
        setIsErr(true);
        setMsg(error);
      });
  };

  const getCodeClinicAPI = () => {
    setIsLoaded(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw =
      '{"token":"SemogaDilancarkanSemuanayaAamiiin", "cmd":"LoadAllDataClinic", "cari":"", "posisi":0,"batas":100}';
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);

        if (res.error === 0) {
          let data = res.setvar.varvalue.map((item) => {
            return {
              code_clinic: item.code,
              nama_clinic: item.name,
            };
          });

          setGetCodeKlinik(data);
        } else {
          console.log(res);
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  const getIdRekamMedisAPI = (id) => {
    setIsLoaded(true);

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw = `{
      "token":"SemogaDilancarkanSemuanayaAamiiin",
      "cmd":"LoadAllDataRekamMedis",
      "cari":"",
      "posisi":0,
      "batas":100
  }`;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);

        if (res.error === 0) {
          let data = res.setvar.varvalue.map((item) => {
            return {
              id_rekam_medis: item.id,
              nama_rekam_medis: item.patient_name,
            };
          });

          setGetIdRekamMedis(data);
        } else {
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  const getIdObatAPI = (id) => {
    setIsLoaded(true);

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw = `{
    "token":"SemogaDilancarkanSemuanayaAamiiin",
    "cmd":"LoadAllDataObat",
    "cari":"",
    "posisi":0,
    "batas":100
}`;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);

        if (res.error === 0) {
          let data = res.setvar.varvalue.map((item) => {
            return {
              id_obat: item.id,
              nama_obat: item.name,
            };
          });

          setGetIdObat(data);
        } else {
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  const getDataResepAPI = () => {
    setIsLoaded(true);

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw = `{
    "token":"SemogaDilancarkanSemuanayaAamiiin",
    "cmd":"LoadIdDataResep",
    "clinic_code":"CN-BDG001",
    "id_rekam_medis":"${idResep}"
}`;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);

        if (res.error === 0) {
          let data = res.setvar.varvalue[0];
          setKodeKlinik(res.clinic_code);
          setIdRekamMedis(res.id_rekam_medis);
          setIdObat(data.id);
          setJumlahObat(data.qyt);
        } else {
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  useEffect(() => {
    getIdRekamMedisAPI();
    getCodeClinicAPI();
    getIdObatAPI();
    getDataResepAPI();
  }, []);

  useEffect(() => {
    addResepData.clinic_code = kodeKlinik;
  }, [addResepData, kodeKlinik]);

  useEffect(() => {
    addResepData.id_rekam_medis = idRekamMedis;
  }, [addResepData, idRekamMedis]);

  useEffect(() => {
    addResepData.id_obat = idObat;
  }, [addResepData, idObat]);

  useEffect(() => {
    addResepData.qty_obat = jumlahObat;
  }, [addResepData, jumlahObat]);

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const handleCloseSnack = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setIsSuccess(false);
    setIsErr(false);
  };

  const succcesMsg = (
    <Snackbar
      open={isSuccess}
      autoHideDuration={6000}
      onClose={handleCloseSnack}
    >
      <Alert onClose={handleCloseSnack} severity="success">
        {msg}
      </Alert>
    </Snackbar>
  );

  const errMsg = (
    <Snackbar open={isErr} autoHideDuration={6000} onClose={handleCloseSnack}>
      <Alert onClose={handleCloseSnack} severity="error">
        {msg}
      </Alert>
    </Snackbar>
  );

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Edit Data Resep
      </Typography>
      <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <TextField
              id="kodeKlinik"
              select
              fullWidth
              label="Kode Klinik"
              value={kodeKlinik}
              onChange={(e) => {
                setKodeKlinik(e.target.value);
              }}
              disabled
            >
              {getCodeKlinik.map((option) => (
                <MenuItem key={option.code_clinic} value={option.code_clinic}>
                  {option.code_clinic} ( {option.nama_clinic})
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              id="idRekamMedis"
              select
              fullWidth
              label="ID Rekam Medis"
              value={idRekamMedis}
              disabled
              onChange={(e) => {
                setIdRekamMedis(e.target.value);
              }}
            >
              {getIdRekamMedis.map((option) => (
                <MenuItem
                  key={option.id_rekam_medis}
                  value={option.id_rekam_medis}
                >
                  {option.id_rekam_medis}
                </MenuItem>
              ))}
            </TextField>
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              id="namaObat"
              select
              fullWidth
              label="Nama Obat"
              value={idObat}
              onChange={(e) => {
                setIdObat(e.target.value);
              }}
              helperText="Pilih Nama Obat"
            >
              {getIdObat.map((option) => (
                <MenuItem key={option.id_obat} value={option.id_obat}>
                  {option.nama_obat}
                </MenuItem>
              ))}
            </TextField>
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              id="jumlahObat"
              fullWidth
              label="Jumlah Obat"
              value={jumlahObat}
              type="number"
              onChange={(e) => setJumlahObat(e.target.value)}
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12}>
            <ButtonGroup
              color="primary"
              aria-label="outlined primary button group"
            >
              <Button
                onClick={() => {
                  history.push("/backend/resep");
                }}
              >
                Cancel
              </Button>
              <Button variant="contained" color="primary" type="submit">
                Save
              </Button>
            </ButtonGroup>
          </Grid>
        </Grid>
      </form>
      {succcesMsg}
      {errMsg}
    </React.Fragment>
  );
}
