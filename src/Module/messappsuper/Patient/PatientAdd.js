import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";

import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { MenuItem } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function DoctorAdd() {
  const classes = useStyles();
  const history = useHistory();
  const [klinikKode, setKlinikKode] = useState("");
  const [getCodeClinic, setGetCodeKlinik] = useState([]);
  const [namaPasien, setNamaPasien] = useState("");
  const [emailPasien, setEmailPasien] = useState("");
  const [tglLahirPasien, setTglLahirPasien] = useState("");
  const [tglRegistrasi, setTglRegistrasi] = useState("");
  const [biometrikPasien, setBiometrikPasien] = useState("");
  const [alamatPasien, setAlamatPasien] = useState("");
  const [jkPasien, setJkPasien] = useState("");
  const [golonganDarahPasien, setGolonganDarahPasien] = useState("");
  const [pekerjaanPasien, setPekerjaanPasien] = useState("");
  const [pendidikanPasien, setPendidikanPasien] = useState("");
  const [statusPasien, setStatusPasien] = useState("");
  const [agamaPasien, setAgamaPasien] = useState("");
  const [waPasien, setWaPasien] = useState("");
  const [isSuccess, setIsSuccess] = useState(false);
  const [isErr, setIsErr] = useState(false);
  const [msg, setMsg] = useState("");
  const [isLoaded, setIsLoaded] = useState(false);
  const { register, handleSubmit } = useForm();

  let addPatientData = {
    wa_number: "",
    clinic_code: "",
    biometrik: "",
    name: "",
    email: "",
    birthdate: "",
    address: "",
    gender: "",
    blood_type: "",
    education: "",
    job: "",
    status: "",
    religion: "",
    register_date: "2020-07-25",
    created_at: "2020-07-25",
  };

  const getCodeClinicAPI = () => {
    setIsLoaded(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw =
      '{"token":"SemogaDilancarkanSemuanayaAamiiin", "cmd":"LoadAllDataClinic", "cari":"", "posisi":0,"batas":100}';
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);

        if (res.error === 0) {
          let data = res.setvar.varvalue.map((item) => {
            return {
              code_clinic: item.code,
              nama_clinic: item.name,
            };
          });

          setGetCodeKlinik(data);
        } else {
          console.log(res);
          setIsErr(true);
          setMsg(res.setvar.varvalue);
        }
        setIsLoaded(true);
      })
      .catch((error) => console.log("error", error));
  };

  const onSubmit = (data) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    const raw = `{
              "token":"SemogaDilancarkanSemuanayaAamiiin",
              "cmd":"IudDataPasien",
              "func":"insert",
              "wa_number":"${addPatientData.wa_number}",
              "clinic_code":"${addPatientData.clinic_code}",
              "biometrik":"${addPatientData.biometrik}",
              "name":"${addPatientData.name}",
              "email":"${addPatientData.email}",
              "birthdate":"${addPatientData.birthdate}",
              "address":"${addPatientData.address}",
              "gender":"${addPatientData.gender}",
              "blood_type":"${addPatientData.blood_type}",
              "education":"${addPatientData.education}",
              "job":"${addPatientData.job}",
              "status":"${addPatientData.status}",
              "religion":"${addPatientData.religion}",
              "register_date":"${addPatientData.register_date}",
              "created_at":"2020-07-25 19:32:06"
            }`;
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    console.log(raw);
    fetch("https://clinic.messapp.id/v2/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);
        console.log(res);

        if (res.error === "0") {
          setIsSuccess(true);
          setMsg(res.setvar.varvalue);
          setTimeout(() => {
            history.push("/pasien");
          }, 2000);
        } else {
          setIsSuccess(true);
          setMsg(res.setvar.varvalue);
        }
      })
      .catch((error) => {
        console.log("error", error);
        setIsErr(true);
        setMsg(error);
      });
  };

  useEffect(() => {
    getCodeClinicAPI();
  }, []);

  useEffect(() => {
    addPatientData.wa_number = waPasien;
  }, [addPatientData, waPasien]);

  useEffect(() => {
    addPatientData.clinic_code = klinikKode;
  }, [addPatientData, klinikKode]);

  useEffect(() => {
    addPatientData.biometrik = biometrikPasien;
  }, [addPatientData, biometrikPasien]);

  useEffect(() => {
    addPatientData.name = namaPasien;
  }, [addPatientData, namaPasien]);

  useEffect(() => {
    addPatientData.email = emailPasien;
  }, [addPatientData, emailPasien]);

  useEffect(() => {
    addPatientData.birthdate = tglLahirPasien;
  }, [addPatientData, tglLahirPasien]);

  useEffect(() => {
    addPatientData.register_date = tglRegistrasi;
  }, [addPatientData, tglRegistrasi]);

  useEffect(() => {
    addPatientData.address = alamatPasien;
  }, [addPatientData, alamatPasien]);

  useEffect(() => {
    addPatientData.gender = jkPasien;
  }, [addPatientData, jkPasien]);

  useEffect(() => {
    addPatientData.blood_type = golonganDarahPasien;
  }, [addPatientData, golonganDarahPasien]);

  useEffect(() => {
    addPatientData.education = pendidikanPasien;
  }, [addPatientData, pendidikanPasien]);

  useEffect(() => {
    addPatientData.job = pekerjaanPasien;
  }, [addPatientData, pekerjaanPasien]);

  useEffect(() => {
    addPatientData.status = statusPasien;
  }, [addPatientData, statusPasien]);

  useEffect(() => {
    addPatientData.religion = agamaPasien;
  }, [addPatientData, agamaPasien]);

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const handleCloseSnack = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setIsSuccess(false);
    setIsErr(false);
  };

  const succcesMsg = (
    <Snackbar
      open={isSuccess}
      autoHideDuration={6000}
      onClose={handleCloseSnack}
    >
      <Alert onClose={handleCloseSnack} severity="success">
        {msg}
      </Alert>
    </Snackbar>
  );

  const errMsg = (
    <Snackbar open={isErr} autoHideDuration={6000} onClose={handleCloseSnack}>
      <Alert onClose={handleCloseSnack} severity="error">
        {msg}
      </Alert>
    </Snackbar>
  );

  const jk = [
    {
      value: "1",
      label: "laki-laki",
    },
    {
      value: "0",
      label: "perempuan",
    },
  ];

  const golonganDarah = [
    {
      value: "A",
      label: "A",
    },
    {
      value: "B",
      label: "B",
    },
    {
      value: "AB",
      label: "AB",
    },
    {
      value: "O",
      label: "O",
    },
  ];

  const status = [
    {
      value: "2",
      label: "Nikah",
    },
    {
      value: "1",
      label: "Lajang",
    },
    {
      value: "3",
      label: "Cerai",
    },
  ];

  const agama = [
    {
      value: "1",
      label: "Islam",
    },
    {
      value: "2",
      label: "Kristen",
    },
    {
      value: "3",
      label: "Hindu",
    },
    {
      value: "4",
      label: "Budha",
    },
    {
      value: "5",
      label: "Konghuchu",
    },
    {
      value: "6",
      label: "Katolik",
    },
  ];

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Tambah Data Pasien
      </Typography>
      <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <TextField
              id="kodeKlinik"
              select
              fullWidth
              label="Kode Klinik"
              value={klinikKode}
              onChange={(e) => {
                setKlinikKode(e.target.value);
              }}
              helperText="Pilih Kode Klinik"
            >
              {getCodeClinic.map((option) => (
                <MenuItem key={option.code_clinic} value={option.code_clinic}>
                  {option.code_clinic} ( {option.nama_clinic} )
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="namaPasien"
              label="Nama Pasien"
              name="nama_pasien"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setNamaPasien(e.target.value)}
              value={namaPasien}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="emailPasien"
              label="Email Pasien"
              name="email_pasien"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setEmailPasien(e.target.value)}
              value={emailPasien}
              autoFocus
              type="email"
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="tanggalLahir"
              label="Tanggal Lahir Pasien"
              name="tanggalLahirPasien"
              type="date"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setTglLahirPasien(e.target.value)}
              value={tglLahirPasien}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="tglRegistrasi"
              label="Tanggal Registrasi Pasien"
              name="tglRegistrasi"
              type="date"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setTglRegistrasi(e.target.value)}
              value={tglRegistrasi}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="biometrik"
              label="Biometrik"
              name="biometrik"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              type="number"
              onChange={(e) => setBiometrikPasien(e.target.value)}
              value={biometrikPasien}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="alamat"
              label="Alamat Pasien"
              name="alamatPasien"
              fullWidth
              margin="normal"
              type="text"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setAlamatPasien(e.target.value)}
              value={alamatPasien}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              id="jenisKelamin"
              select
              fullWidth
              label="Select"
              value={jkPasien}
              onChange={(e) => setJkPasien(e.target.value)}
              helperText="Pilih Jenis Kelamin"
            >
              {jk.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              id="golonganDarah"
              select
              fullWidth
              label="Select"
              value={golonganDarahPasien}
              onChange={(e) => setGolonganDarahPasien(e.target.value)}
              helperText="Pilih Golongan Darah"
            >
              {golonganDarah.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="pendidikan"
              label="Pendidikan Pasien"
              name="pendidikanPasien"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setPendidikanPasien(e.target.value)}
              value={pendidikanPasien}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="pekerjaan"
              label="Pekerjaan Pasien"
              name="pekerjaanPasien"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setPekerjaanPasien(e.target.value)}
              value={pekerjaanPasien}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              id="statusPasien"
              select
              fullWidth
              label="Select"
              value={statusPasien}
              onChange={(e) => setStatusPasien(e.target.value)}
              helperText="Pilih Status"
            >
              {status.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              id="agamaPasien"
              select
              fullWidth
              label="Select"
              value={agamaPasien}
              onChange={(e) => setAgamaPasien(e.target.value)}
              helperText="Pilih Agama"
            >
              {agama.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="noWa"
              label="Nomer Wa"
              name="nomerWa"
              fullWidth
              margin="normal"
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setWaPasien(e.target.value)}
              value={waPasien}
              autoFocus
              ref={register({
                required: "Required",
              })}
            />
          </Grid>

          <Grid item xs={12}>
            <ButtonGroup
              color="primary"
              aria-label="outlined primary button group"
            >
              <Button
                onClick={() => {
                  history.push("/backend/pasien");
                }}
              >
                Cancel
              </Button>
              <Button variant="contained" color="primary" type="submit">
                Save
              </Button>
            </ButtonGroup>
          </Grid>
        </Grid>
      </form>
      {succcesMsg}
      {errMsg}
    </React.Fragment>
  );
}
