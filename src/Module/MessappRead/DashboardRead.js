import React from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Copyright from "../../component/Copyright";
import { Switch, Route } from "react-router-dom";
import DrawerRead from "./DrawerRead";
import ApplicationBar from "../../component/ApplicationBar";
import User from "../messappsuper/User/User";
import Clinic from "../messappsuper/Clinic/Clinic";
import Doctor from "../messappsuper/Doctor/Doctor";
import Patient from "../messappsuper/Patient/Patient";
import Consultation from "../messappsuper/Consultation/Consultation";
import SettingQueue from "../messappsuper/SettingQueue/SettingQueue";
import Action from "../messappsuper/Action/Action";
import ConsultationQueue from "../messappsuper/ConsultationQueue/ConsultationQueue";
import Medicine from "../messappsuper/Medicine/Medicine";
import RekamMedis from "../messappsuper/RekamMedis/RekamMedis";
import Resep from "../messappsuper/Resep/Resep";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: "none",
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
}));

export default function DashboardRead(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  return (
    <Switch>
      <Route path="/user">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerRead open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <User />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
      <Route path="/user-otoritas">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerRead open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <User />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
      <Route exact path="/klinik">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerRead open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <Clinic />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
      <Route exact path="/dokter">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerRead open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <Doctor />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
      
      <Route exact path="/pasien">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerRead open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <Patient />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
      <Route exact path="/konsultasi">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerRead open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <Consultation />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
      <Route exact path="/setting-antrian/">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerRead open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <SettingQueue />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
      
      <Route exact path="/tindakan">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerRead open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <Action />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
      <Route exact path="/antrian">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerRead open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <ConsultationQueue />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
      <Route exact path="/obat">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerRead open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <Medicine />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
      <Route exact path="/rekam-medis">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerRead open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <RekamMedis />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
      <Route exact path="/resep">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerRead open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <Resep />
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
      <Route path="/">
        <div className={classes.root}>
          <CssBaseline />
          <ApplicationBar open={open} setOpen={setOpen} />
          <DrawerRead open={open} setOpen={setOpen} />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                {/* Chart */}
                <Grid item xs={12} md={8} lg={9}>
                  <Paper className={fixedHeightPaper}>Menu</Paper>
                </Grid>
                {/* Recent Deposits */}
                <Grid item xs={12} md={4} lg={3}>
                  <Paper className={fixedHeightPaper}>Menu</Paper>
                </Grid>
                {/* Recent Orders */}
                <Grid item xs={12}>
                  <Paper className={classes.paper}>Menu</Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </div>
      </Route>
    </Switch>
  );
}
