import React, { useState } from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { useHistory } from "react-router-dom";

import Drawer from '@material-ui/core/Drawer';
import ListItemIcon from '@material-ui/core/ListItemIcon';

import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import MotorcycleIcon from '@material-ui/icons/Motorcycle';


export default function DrawerSuper(props) {
  const history = useHistory();
  const [isDrawerOpen, setIsDrawerOpen] = useState(false)
  
  return (
    <Drawer anchor="left" open={props.isDrawerOpen} onClose={() => {
        setIsDrawerOpen(!isDrawerOpen)
        props.setIsDrawerOpen(false)
    }}>
        <List>
            <ListItem button key="Operator" 
                    onClick={() => { 
                    setIsDrawerOpen(!isDrawerOpen) 
                    props.setIsDrawerOpen(false)
                    history.push("/operator");
                    }} >
                <ListItemIcon><EmojiPeopleIcon /></ListItemIcon>
                <ListItemText primary="Operator" />
            </ListItem>
            <ListItem button key="Driver" onClick={() => { 
                    setIsDrawerOpen(!isDrawerOpen) 
                    props.setIsDrawerOpen(false)
                    history.push("/driver");
                    }}>
                <ListItemIcon><MotorcycleIcon /></ListItemIcon>
                <ListItemText primary="Driver" />
            </ListItem>
        </List>
    </Drawer>        
  );
}
