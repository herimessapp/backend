import React, { useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { useForm } from "react-hook-form";
import Copyright from "./component/Copyright";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function LoginEmail(props) {
  const classes = useStyles();
  const { register, handleSubmit } = useForm();
  const [email, setEmail] = useState("");
  const [isErr, setIsErr] = useState(false);
  const [msg, setMsg] = useState("");
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setIsErr(false);
  };
  const errMsg = (
    <Snackbar open={isErr} autoHideDuration={6000} onClose={handleClose}>
      <Alert onClose={handleClose} severity="error">
        {msg}
      </Alert>
    </Snackbar>
  );

  const onSubmit = (data) => {
    // TO DO CHECK IS EXIST
    var raw =
      '{"token":"SemogaDilancarkanSemuanayaAamiiin","cmd":"LoadEmailDataUser",' +
      '"email":"' +
      email +
      '",' +
      '"ndata":""}';
    var requestOptions = {
      method: "POST",
      body: raw,
      redirect: "follow",
    };
    fetch("https://wajek.messapp.id/v3/index.php", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let res = JSON.parse(result);
        if (res.error === 0) {
          props.setUser({
            email: res.setvar.varvalue.email,
            wa: res.setvar.varvalue.wa,
            role: "messappcrud",
            otp: props.user.otp,
          });
        } else {
          setMsg(res.setvar.varvalue);
          setIsErr(true);
        }
      })
      .catch((error) => console.log(JSON.stringify(error)));
  };
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={(e) => {
              setEmail(e.target.value);
            }}
            type="email"
            ref={register({
              required: "Required",
              pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                message: "invalid email address",
              },
            })}
            helperText=""
            error={false}
            value={email}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Get OTP
          </Button>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
      {errMsg}
    </Container>
  );
}
